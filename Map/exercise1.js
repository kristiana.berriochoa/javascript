//EXERCISE 1:
//Create a function called "objToMap".
//It takes two objects as arguments: one object to take propreties from, and a schema object.
//Return a new map with all propreties from one of the objects, which meet our conditions.
//Conditions:
//- The key of the object we our trying to clone must be present in our schema object.
//- All values of the object must be numbers, or convertible to numbers.
//========================= Example ========================= 
//objToMap({name: 'Mike', age: 22, height: '170cm', sibilings: '3'}, {name: undefined, height: undefined, sibilings: undefined});
//Only sibilings meet the condition because it is the only element which is a number, or convertible to one (also present in our schema).
//Map(1) =====> {"sibilings" =====> "3"}

var obj = {name: 'Mike', age: 22, height: '170cm', sibilings: '3'};
var schema = {name: undefined, height: undefined, sibilings: undefined};

function objToMap(obj, schema){
    schema.forEach(function(key){
        if(key in obj && typeof obj[key] == Number(obj[key])){
            const myMap = new Map()
        }
    })
    return 
}

module.exports = { objToMap }
