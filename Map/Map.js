//************************************* MAP ******************************************

//EXERCISE 1:
//Create a function called "objToMap".
//It takes two objects as arguments: one object to take propreties from, and a schema object.
//Return a new map with all propreties from one of the objects, which meet our conditions.
//Conditions:
//- The key of the object we our trying to clone must be present in our schema object.
//- All values of the object must be numbers, or convertible to numbers.
//========================= Example ========================= 
//objToMap({name: 'Mike', age: 22, height: '170cm', sibilings: '3'}, {name: undefined, height: undefined, sibilings: undefined});
//Only "sibilings" meets the conditions; it's the only element which is a number, or convertible (present in our schema).
//Map(1) {"sibilings" => "3"}


//EXERCISE 2:
//Create a function called "find_expensive_items". 
//It takes two arguments: a Map and a threshold price -- the Map is a shopping list.
//Return a new Map with two totals: the sum of all items more than or equal to the threshhold, and the sum of those less than it.
//========================= Example ========================= 
//find_expensive_items(new Map([['bread', 10], ['shampoo', 20], ['banana', 34], ['cheese', 12]]), 15);
//Map(2) {"expensive" => 54, "cheap" => 22}


//EXERCISE 3:
//Create a function called "mostTimes", which takes an array as argument.
//Return a new Map where the key is the element of the map, and the value is how many times it was found.
//For this exercise you are not allowed to use objects.
//========================= Example ========================= 
//mostTimes([2, 90, 90, 4, 8, 23, 23])
//Map(5) {2 => 1, 90 => 2, 4 => 1, 8 => 1, 23 => 2}


//EXERCISE 4:
//Create a function called "howManyGifts".
//It takes a Map as the first parameter, and a budget as the second.
//Your task is to get the max number of gifts within the budget.
//Return a new Map with the total amount spent and the gifts purchased.
//========================= Example ========================= 
//gifts =====> Map(4) {"ps4" => 300, "game" => 50, "razor" => 190, "speaker" => 58}
//howManyGifts(100, gifts) =====> Map(1) {"game" => 50}
//howManyGifts(400, gifts) =====> Map(2) {"ps4" => 300, "game" => 50}