//EXERCISE 4:
//Create a function called "howManyGifts".
//It takes a Map as the first parameter, and a budget as the second.
//Your task is to get the max number of gifts within the budget.
//Return a new Map with the total amount spent and the gifts purchased.
//========================= Example ========================= 
//gifts =====> Map(4) {"ps4" => 300, "game" => 50, "razor" => 190, "speaker" => 58}
//howManyGifts(100, gifts) =====> Map(1) {"game" => 50}
//howManyGifts(400, gifts) =====> Map(2) {"ps4" => 300, "game" => 50}

function howManyGifts() {

}

module.exports = {
    howManyGifts
}