//EXERCISE 2:
//Create a function called "find_expensive_items". 
//It takes two arguments: a Map and a threshold price -- the Map is a shopping list.
//Return a new Map with two totals: the sum of all items more than or equal to the threshhold, and the sum of those less than it.
//========================= Example ========================= 
//find_expensive_items(new Map([['bread', 10], ['shampoo', 20], ['banana', 34], ['cheese', 12]]), 15);
//Map(2) {"expensive" => 54, "cheap" => 22}

function find_expensive_items(){

}

module.exports = {
	find_expensive_items
}