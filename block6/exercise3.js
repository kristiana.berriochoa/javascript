//EXERCISE 3:
//Write a function called "modifyObject", which takes three arguments. 
//- The first argument is an object, to add new data.
//- The second argument is a key, to be added.
//- The third argument is a value, to be associated with the key.
//========================= Example ========================= 
//var obj = {a: 1, b: 2} =====> The original object
//modifyObject(obj, 'c', 3) =====> {a: 1, b: 2, c: 3}

var obj = {a: 1, b: 2};

function modifyObject(obj, newKey, amount){
    obj[newKey] = amount;
    return obj;
}

module.exports = {
    obj, modifyObject
}