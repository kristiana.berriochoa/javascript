//EXERCISE 5:
//Write a function called "swap" that takes one argument: an object.
//Return another object where the key/value pairs have been swapped. 
//The original object should not be modified.
//========================= Example =========================
//var obj = {a: 1, b: 2}
//var newObje = swap(obj) =====> newObje =====> {1: 'a', 2: 'b'}

var obj = {a: 1, b: 2};
var newObje = swap(obj);

function swap(obj){
    var newObje = {};
    for(var key in obj){
        newObje[obj[key]] = key
    }
    return newObje;
}

module.exports = {
    swap
}