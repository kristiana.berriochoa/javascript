//EXERCISE 12: Schema force with Default
//We will now make sure that missing values are defaulted to a certain value. 
//Now, the value of a given property of the schema object will be formed by an object with keys "type" and "default". 
//The type-setting system from the previous exercise works in the same way, with the exception of the new syntax. 
//If no default key is added, the given property will not be added, if missing. 
//If an input is not present, or breaking another of the schema rules, the value will be set to the default.
//========================= Examples =========================
/*schema = {
    name: {type: "string", default: "NoBody"},
    age: {type: "number"},
    married: {type: "boolean", default: false}
}*/
//DB = [];
//model("add", {id: 1, name: "pedro", age: "32", address: "Muntaner 262, Barcelona, Spain"})
//DB =====> [{name: "Pedro", married: false}] => 'married' set to default even if missing 

//model("add", {name: 43, married: "asdfasdf"})
//DB =====> [{name: "Pedro", married: false}, {name: "NoBody", married: false}]
//=> 'married' and 'name' set to default even wrong type
 
//model("add", {name: "43", married: true, age: 20})
//DB =====> [{name: "Pedro", married: false}, {name: "NoBody", married: false}, {name: "43", married: true, age: 20}] 
//=> 'married' and 'name' set to default

var schema = {
    name: {type: "string", default: "NoBody"},
    age: {type: "number"},
    married: {type: "boolean", default: false}
};

function model(action, obj, schema){
    var DB = [];
    if(action == 'add'){
        var newObj = {};
        for(var key in obj){
            if(key in schema && typeof obj[key] == schema[key]['type']){
                newObj[key] = obj[key];
            }
        }
        for(var key in schema){
            if(!(key in newObj) && schema[key]['default'] != undefined){
                newObj[key] = schema[key]['default'];
            }
        }
        DB.push(newObj);
    } else {
        return "missing action argument or wrong provided";
    }
    return DB;
}

module.exports = {
    model
}