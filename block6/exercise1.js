//EXERCISE 1:
/*
var obj = {
        name: antonello
        lastname: sanna
}
*/
//FIX THE ABOVE CODE SO THAT IT DOESN'T THROW AN ERROR!

var obj = {
    name: 'antonello',
    lastname: 'sanna'
}

module.exports = {
    obj
}