//EXERCISE 13:
//Write an object called "bankAccount" with three functions: withdrawl, deposit, and balance. 
//They keep track of an amount added and removed from a bank account when calling the bankAccount function.
//It will keep it in a 'bankAccount.total'.
//The 'balance' function takes no argument.
//The 'withdrawl' and 'deposit' functions only take one argument.
//========================= Examples =========================
//bankAccount.withdrawl(2)
//bankAccount.withdrawl(5)
//bankAccount.deposit(4)
//bankAccount.deposit(1)
//bankAccount.balance() =====> -2

let bankAccount = {
    total: 0,
    balance: function(){
        return this.total;
    },
    withdraw: function(amount){
        this.total = this.total - amount;
        return this.total;
    },
    deposit: function(amount){
        this.total = this.total + amount;
        return this.total;
    }
}

module.exports = {
    bankAccount
}