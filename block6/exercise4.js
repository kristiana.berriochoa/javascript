//EXERCISE 4:
//Write a function called "addToList" which takes one argument: an array of movie titles.
//Then create a new, empty array called "movieList".
//Loop through the array of movie titles and push them to the movieList array as objects.
//Each object will have two key/value pairs with titles and ids: 
//- The movie title from the array will be the value of the 'title' key in the object.
//- The element index from the array will be the value of the 'id' key in the object.
//Return the movieList.
//========================= Example =========================
//var movies = ['matrix', 'the dark knight', 'a beautiful mind', 'american pie']
//addToList(movies) =====> 
//['{title: matrix, id: 0}', '{title: the dark knight, id: 1}', '{title: a beautiful mind, id: 2}', '{title: american pie, id: 3}']

var movies = ['matrix', 'the dark knight', 'a beautiful mind', 'american pie'];

function addToList(movies){
    var movieList = [];
    movies.forEach(function(element,index){
        var obj = {};
        obj['title'] = element;
        obj['id'] = index;
        movieList.push(obj);
    })
    return movieList;
}

module.exports = {
    addToList
}