//EXERCISE 7:
//Write a function called "runOnRange" which takes one argument: an object.
//This object will contain three properties: a start, an end, and a step.
//According to these properties, push specific numbers to an array.
//Return the array.
//========================= Examples =========================
//runOnRange({start: 10, end: 17, step: 3}) =====> 10, 13, 16 
//runOnRange({start: -6, end: -4}) =====> -6, -5, -4 
//runOnRange({start: 12, end: 12}) =====> nothing should be console.logged in this case! 
//runOnRange({start: 23, end: 26, step: -1}) =====> nothing should be console.logged in this case! 
//runOnRange({start: 26, end: 24, step: -1}) =====> -26, -25, -24
//runOnRange({start: 23, end: 26, step: 0}) =====> nothing should be console.logged in this case!

function runOnRange(obj){
    var arr = [];
    var a = obj['start'];
    var b = obj['end'];
    var c = obj['step'];
    if(c == undefined || c == 0){
        return arr;
    }
    if(a > b && c < 1){
        if(c < 0){
            c = Math.abs(c);
        }
        for(var i = a; i >= b; i = i - c){
            arr.push(i);
        }
    } else {
        for(var i = a; i <= b; i = i + c){
            arr.push(i);
        }
    }
    return arr;
}

module.exports = {
    runOnRange
}