//************************************* BLOCK 6 ******************************************

//EXERCISE 1:
/*
var obj = {
        name: antonello
        lastname: sanna
}
*/
//FIX THE ABOVE CODE SO THAT IT DOESN'T THROW AN ERROR!


//EXERCISE 2:
//Write a function called "addToObj" which takes two arguments.
//Return an object which has the first argument as a key and the second as a value.
//========================= Example ========================= 
//addToObj('jason', 'bourne') =====> {jason :  bourne}


//EXERCISE 3:
//Write a function called "modifyObject", which takes three arguments. 
//- The first argument is an object, to add new data.
//- The second argument is a key, to be added.
//- The third argument is a value, to be associated with the key.
//========================= Example ========================= 
//var obj = {a: 1, b: 2} =====> The original object
//modifyObject(obj, 'c', 3) =====> {a: 1, b: 2, c: 3}  


//EXERCISE 4:
//Write a function called "addToList" which takes one argument: an array of movie titles.
//Then create a new, empty array called "movieList".
//Loop through the array of movie titles and push them to the movieList array as objects.
//Each object will have two key/value pairs with titles and ids: 
//- The movie title from the array will be the value of the 'title' key in the object.
//- The element index from the array will be the value of the 'id' key in the object.
//Return the movieList.
//========================= Example =========================
//var movies = ['matrix', 'the dark knight', 'a beautiful mind', 'american pie']
//addToList(movies) =====> 
//['{title: matrix, id: 0}', '{title: the dark knight, id: 1}', '{title: a beautiful mind, id: 2}', '{title: american pie, id: 3}']


//EXERCISE 5:
//Write a function called "swap" that takes one argument: an object.
//Return another object where the key/value pairs have been swapped. 
//The original object should not be modified.
//========================= Example =========================
//var obj = {a: 1, b: 2}
//var newObje = swap(obj) =====> newObje =====> {1: 'a', 2: 'b'} 


//EXERCISE 6:
//Write a function called "getIndex" which finds the index of an element in an array of objects. 
//The objects have multiple key/value pairs so the function needs to be flexible enough to find any one of them.
//Please don't Google "how to find the index of an object in javascript" or similar.
//You cannot use the ES6 method "findIndex" for this exercise!
//Return the index of the found element or -1 if it is not there.
//To pass the test the function should take three arguments: an array of objects, name of the key, and content of the value to search. 
//If they match, return the index of the object containing this matching "key: value" pair.
//========================= Example =========================
//var arr = [{name: 'Antonello', location: 'Barcelona'}, {email: 'george@barcelonacodeschool.com', name: 'George'}, {name: 'Golda', coder: true}];
//getIndex(arr, 'name', 'Antonello') =====> 0, (the index of the object containing the provided 'key: value' pair)


//EXERCISE 7:
//Write a function called "runOnRange" which takes one argument: an object.
//This object will contain three properties: a start, an end, and a step.
//According to these properties, push specific numbers to an array.
//Return the array.
//========================= Examples =========================
//runOnRange({start: 10, end: 17, step: 3}) =====> 10, 13, 16 
//runOnRange({start: -6, end: -4}) =====> -6, -5, -4 
//runOnRange({start: 12, end: 12}) =====> nothing should be console.logged in this case! 
//runOnRange({start: 23, end: 26, step: -1}) =====> nothing should be console.logged in this case! 
//runOnRange({start: 26, end: 24, step: -1}) =====> -26, -25, -24
//runOnRange({start: 23, end: 26, step: 0}) =====> nothing should be console.logged in this case!


//EXERCISE 8:
//Write a function called "last" which takes one argument: an object.
//Return an object containing only the last 'key: value' pair and does not modify the original object.
//========================= Example =========================
//var obj = {a: 1, b: 2}
//var newObje = last(obj) =====> newObje =====> {b: 2}; obj =====> {a: 1, b: 2}


//EXERCISE 9:
//Write a function called "sumAll" which takes one argument: an object.
//Sum all it's values. 
//If no object is provided, or the object is empty return 0.
//========================= Examples =========================
//var obj = {a: 1, b: 2, c: 2}
//sumAll(obj) =====> 5 
//sumAll({}) =====> 0 
//sumAll() =====> 0 


//EXERCISE 10:
//Create a function called "model". 
//It allows you to control how you interact with a collection of objects, that we call a collection, stored in the DB. 
//It imposes conditions on the properties of the collection; for example, which keys are allowed.
//It uses the definitions of a predefined object called schema (you don't need to define it).
//It asserts the keys that each object in a collection are allowed. 
//The model function can take three arguments:
//- The first is the type of operation you want to execute on the DB.
//- The second is the data needed to execute it.
//- The third is the schema object defining the pattern or template for which data is valid to add to the DB. 
//For now, only define an operation called 'add'.
//If the argument "add" is not present then nothing should be added.
//========================= Example Schema =========================
//schema = ["id", "name", "age"]
//Please note the schema is only an example here; I may be calling a different one for the test.
//=========================
//Define the DB array inside the function to avoid issues.
//It should take three argument: an action, an object, and a schema.
//The action should be "add", or no changes should be done.
//========================= Examples =========================
//var DB = []
//model("add", {id: 1, name: "Joe", age: 32, address: "Muntaner 262, Barcelona"}, schema)
//DB =====> [{id: 1, name: "Joe", age: 32}] => Address was not added because not allowed by the schema 

//model("add", {id: 1, age: 32, address: "Muntaner 262, Barcelona"}, schema)
//DB =====> [{id: 1, age: 32}] => Address was not added because not allowed by the schema 


//EXERCISE 11:
//Continuing with the previous exercise, add the possibility to force the type of the value to be set on a given property. 
//This case schema is an object and no longer an array. 
//The allowed values are only a "string", a "number", and "boolean". 
//If the value of a given property is not the appropriate type, then the property will not be added to the new object in the DB.
//If the argument "add" is not present then nothing should be added.
//========================= Example =========================
//schema = {id: "number", name: "string", age: "number", married: "boolean"};
//DB = [];
//model("add", {id: 1, name: "Joe", age: "32", address: "Muntaner 262, Barcelona", married: "to Mary"})
//DB =====> [{id: 1, name: "Joe"}] => married and age not added because of the wrong type (Why?)


//EXERCISE 12: Schema force with Default
//We will now make sure that missing values are defaulted to a certain value. 
//Now, the value of a given property of the schema object will be formed by an object with keys "type" and "default". 
//The type-setting system from the previous exercise works in the same way, with the exception of the new syntax. 
//If no default key is added, the given property will not be added, if missing. 
//If an input is not present, or breaking another of the schema rules, the value will be set to the default.
//========================= Examples =========================
/*schema = {
    name: {type: "string", default: "NoBody"},
    age: {type: "number"},
    married: {type: "boolean", default: false}
}*/
//DB = [];
//model("add", {id: 1, name: "pedro", age: "32", address: "Muntaner 262, Barcelona, Spain"})
//DB =====> [{name: "Pedro", married: false}] => 'married' set to default even if missing 

//model("add", {name: 43, married: "asdfasdf"})
//DB =====> [{name: "Pedro", married: false}, {name: "NoBody", married: false}]
//=> 'married' and 'name' set to default even wrong type
 
//model("add", {name: "43", married: true, age: 20})
//DB =====> [{name: "Pedro", married: false}, {name: "NoBody", married: false}, {name: "43", married: true, age: 20}] 
//=> 'married' and 'name' set to default


//EXERCISE 13:
//Write an object called "bankAccount" with three functions: withdrawl, deposit, and balance. 
//They keep track of an amount added and removed from a bank account when calling the bankAccount function.
//It will keep it in a 'bankAccount.total'.
//The 'balance' function takes no argument.
//The 'withdrawl' and 'deposit' functions only take one argument.
//========================= Examples =========================
//bankAccount.withdrawl(2)
//bankAccount.withdrawl(5)
//bankAccount.deposit(4)
//bankAccount.deposit(1)
//bankAccount.balance() =====> -2


//EXERCISE 14:
//Write a function called "splice".
//Which extrapolates properties from an object and uses them for a newly created object.
//This function takes three arguments: the first is an object, the second and third are numbers.
//The second argument indicates the position of the 'key: value' pair to start taking properties from (splicing).
//The third argument indicates how many to remove. 
//For instance, splice(0, 2) refers to the positions 0 and 1 of following object: var obj = {a: 1, b: 2, c: 2}.
//Calling "splice" with the arguments (obj, 0, 2) should return {a: 1, b: 2}.
//=========================
//splice(obj, 0, 2) =====> {a: 1, b: 2}
//Assume that, as it is common in computer science, the first element of an object is 0 and not 1.
//If the third argument is not passed it should default to 1.
//========================= Examples =========================
//var obj = {a: 1, b: 2, c: 2}
//var newObje = splice(obj, 0, 2) =====> newObje =====> {a: 1, b: 2} 
//var newObje = splice(obj, 2, 2) =====> newObje =====> {c: 2} 
//var newObje = splice(obj, 5, 2) =====> newObje =====> { } 
//var newObje = splice(obj, 0) =====> newObje =====> {a: 1} 
//var newObje = splice(obj, 0, 0) =====> newObje =====> { }


//EXERCISE 15:
//Write a function called "sort" which sorts the values of an object, which should all be numbers. 
//Sort the object numericaly, in ascending order.
//========================= Example =========================
//sort({a: 1, b: 20, c: 3, d: 4, e: 1, f: 4}) =====> {a: 1, e: 1, c: 3, d: 4, f: 4, b:20}


//EXERCISE 16:
//Extend the functionality of the previous function "sort".
//Include the possibility of having different types of sorting according to the arguments passed. 
//Decide to sort by keys or values and if in ascending or descending order.
//Please note that the keys are letters and values are numbers; sorting by key needs to sort alphabetically.
//========================= Examples =========================
//var obj = {a: 1, b: 20, c: 3, d: 4, e: 1, f: 4}
//sort(obj, 'values', 'ascending') =====> Object {a: 1, e: 1, c: 3, d: 4, f: 4, b: 20}
//sort(obj, 'values', 'descending') =====> Object {b: 20, d: 4, f: 4, c: 3, a: 1, e: 1}
//sort(obj,'keys','ascending') =====> Object {a: 1, b: 20, c: 3, d: 4, e: 1, f: 4}
//sort(obj,'keys','descending') =====> Object {f: 4, e: 1, d: 4, c: 3, b: 20, a: 1}
//Note: should the second or third argument be missing console.log the following message: "missing argument here!".