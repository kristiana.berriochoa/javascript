//EXERCISE 14:
//Write a function called "splice".
//Which extrapolates properties from an object and uses them for a newly created object.
//This function takes three arguments: the first is an object, the second and third are numbers.
//The second argument indicates the position of the 'key: value' pair to start taking properties from (splicing).
//The third argument indicates how many to remove. 
//For instance, splice(0, 2) refers to the positions 0 and 1 of following object: var obj = {a: 1, b: 2, c: 2}.
//Calling "splice" with the arguments (obj, 0, 2) should return {a: 1, b: 2}.
//=========================
//splice(obj, 0, 2) =====> {a: 1, b: 2}
//Assume that, as it is common in computer science, the first element of an object is 0 and not 1.
//If the third argument is not passed it should default to 1.
//========================= Examples =========================
//var obj = {a: 1, b: 2, c: 2}
//var newObje = splice(obj, 0, 2) =====> newObje =====> {a: 1, b: 2} 
//var newObje = splice(obj, 2, 2) =====> newObje =====> {c: 2} 
//var newObje = splice(obj, 5, 2) =====> newObje =====> { } 
//var newObje = splice(obj, 0) =====> newObje =====> {a: 1} 
//var newObje = splice(obj, 0, 0) =====> newObje =====> { }

var obj = {a: 1, b: 2, c: 2}; 

function splice(obj, position, amount){
    var count = 0;
    var newObje = {};
    for(var key in obj){ 
        if(count <= amount && count >= position){
            newObje[key] = obj[key];
        }
        if(position == amount){
            if((count - 1) == amount){
                newObje[key] = obj[key];
            }
        }
        count++;
    }
    return newObje;
}

module.exports = {
    splice
}