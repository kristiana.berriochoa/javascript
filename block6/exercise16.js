//EXERCISE 16:
//Extend the functionality of the previous function "sort".
//Include the possibility of having different types of sorting according to the arguments passed. 
//Decide to sort by keys or values and if in ascending or descending order.
//Please note that the keys are letters and values are numbers; sorting by key needs to sort alphabetically.
//========================= Examples =========================
//var obj = {a: 1, b: 20, c: 3, d: 4, e: 1, f: 4}
//sort(obj, 'values', 'ascending') =====> Object {a: 1, e: 1, c: 3, d: 4, f: 4, b: 20}
//sort(obj, 'values', 'descending') =====> Object {b: 20, d: 4, f: 4, c: 3, a: 1, e: 1}
//sort(obj,'keys','ascending') =====> Object {a: 1, b: 20, c: 3, d: 4, e: 1, f: 4}
//sort(obj,'keys','descending') =====> Object {f: 4, e: 1, d: 4, c: 3, b: 20, a: 1}
//Note: should the second or third argument be missing console.log the following message: "missing argument here!".

var obj = {a: 1, b: 20, c: 3, d: 4, e: 1, f: 4};

function sort(obj, sortBy, sortOrder){
    var type = sortBy;
    var order = sortOrder;
    var sortAction = order + type;
    var arr = [];
    var newObje = {};
    if(sortBy == undefined || sortOrder == undefined){
        return `missing argument here!`;
    }
    for(var key in obj){
        arr.push([key, obj[key]]);
    }
    let sorter = {
        ascendingvalues: function(arr){
            arr.sort(function(a, b){
                return a[1] - b[1];
            })
        },
        descendingvalues: function(arr){
            arr.sort(function(a, b){
                return b[1] - a[1];
            })
        },
        ascendingkeys: function(arr){
            arr.sort();
        },
        descendingkeys: function(arr){
            arr.sort();
            arr.reverse();
        }
    }
    sorter[sortAction](arr);
    arr.forEach(function(element){
        newObje[element[0]] = element[1]
    });
    return newObje;
}

module.exports = {
    sort
}