//EXERCISE 8:
//Write a function called "last" which takes one argument: an object.
//Return an object containing only the last 'key: value' pair and does not modify the original object.
//========================= Example =========================
//var obj = {a: 1, b: 2}
//var newObje = last(obj) =====> newObje =====> {b: 2}; obj =====> {a: 1, b: 2}

var obj = {a: 1, b: 2};
var newObje = last(obj);

function last(obj){
    console.log(obj);
    var newObje = {};
    for(var key in obj){  
    }
    newObje[key] = obj[key];
    return newObje;
}

module.exports = {
    last
}