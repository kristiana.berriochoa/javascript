//EXERCISE 15:
//Write a function called "sort" which sorts the values of an object, which should all be numbers. 
//Sort the object numericaly, in ascending order.
//========================= Example =========================
//sort({a: 1, b: 20, c: 3, d: 4, e: 1, f: 4}) =====> {a: 1, e: 1, c: 3, d: 4, f: 4, b:20}

function sort(obj){
    console.log(obj);
    var arr = [];
    var newObje = {};
    for(var key in obj){
        arr.push([key, obj[key]]);
    }
    arr.sort(function(a, b){
        return a[1] - b[1];
    })
    arr.forEach(function(element){
        newObje[element[0]] = element[1]
    })
    return newObje;
}

module.exports = {
    sort
}