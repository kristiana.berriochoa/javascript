//EXERCISE 2:
//Write a function called "addToObj" which takes two arguments.
//Return an object which has the first argument as a key and the second as a value.
//========================= Example ========================= 
//addToObj('jason', 'bourne') =====> {jason :  bourne}

function addToObj(name, surname){
    var obj = {};
    obj[name] = surname;
    return obj;
}

module.exports = {
    addToObj
}