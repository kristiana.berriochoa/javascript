//EXERCISE 10:
//Create a function called "model". 
//It allows you to control how you interact with a collection of objects, that we call a collection, stored in the DB. 
//It imposes conditions on the properties of the collection; for example, which keys are allowed.
//It uses the definitions of a predefined object called schema (you don't need to define it).
//It asserts the keys that each object in a collection are allowed. 
//The model function can take three arguments:
//- The first is the type of operation you want to execute on the DB.
//- The second is the data needed to execute it.
//- The third is the schema object defining the pattern or template for which data is valid to add to the DB. 
//For now, only define an operation called 'add'.
//If the argument "add" is not present then nothing should be added.
//========================= Example Schema =========================
//schema = ["id", "name", "age"]
//Please note the schema is only an example here; I may be calling a different one for the test.
//=========================
//Define the DB array inside the function to avoid issues.
//It should take three argument: an action, an object, and a schema.
//The action should be "add", or no changes should be done.
//========================= Examples =========================
//var DB = []
//model("add", {id: 1, name: "Joe", age: 32, address: "Muntaner 262, Barcelona"}, schema)
//DB =====> [{id: 1, name: "Joe", age: 32}] => Address was not added because not allowed by the schema

//model("add", {id: 1, age: 32, address: "Muntaner 262, Barcelona"}, schema)
//DB =====> [{id: 1, age: 32}] => Address was not added because not allowed by the schema

var schema = ["id", "name", "age"];

function model(action, obj, schema){
    var DB = [];
    if(action == 'add'){
        var newObj = {};
        schema.forEach(function(key){
            if(key in obj){
                newObj[key] = obj[key];
            }
        })
        DB.push(newObj);
    } else {
        return "missing action argument or wrong provided";
    }
    return DB;
}

module.exports = {
    model
}