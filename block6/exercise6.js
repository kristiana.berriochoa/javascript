//EXERCISE 6:
//Write a function called "getIndex" which finds the index of an element in an array of objects. 
//The objects have multiple key/value pairs so the function needs to be flexible enough to find any one of them.
//Please don't Google "how to find the index of an object in javascript" or similar.
//You cannot use the ES6 method "findIndex" for this exercise!
//Return the index of the found element or -1 if it is not there.
//To pass the test the function should take three arguments: an array of objects, name of the key, and content of the value to search. 
//If they match, return the index of the object containing this matching "key: value" pair.
//========================= Example =========================
//var arr = [{name: 'Antonello', location: 'Barcelona'}, {email: 'george@barcelonacodeschool.com', name: 'George'}, {name: 'Golda', coder: true}];
//getIndex(arr, 'name', 'Antonello') =====> 0, (the index of the object containing the provided "key: value" pair)

var arr = [{name: 'Antonello', location: 'Barcelona'}, {email: 'george@barcelonacodeschool.com', name: 'George'}, {name: 'Golda', coder: true}];

function getIndex(arr, key, value){
    var index = -1;
    for(var i = 0; i < arr.length; i++){
        if(arr[i][key] == value){
            index = i;
        }
    }
    return index;
}

module.exports = {
    getIndex
}