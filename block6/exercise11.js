//EXERCISE 11:
//Continuing with the previous exercise, add the possibility to force the type of the value to be set on a given property. 
//This case schema is an object and no longer an array. 
//The allowed values are only a "string", a "number", and "boolean". 
//If the value of a given property is not the appropriate type, then the property will not be added to the new object in the DB.
//If the argument "add" is not present then nothing should be added.
//========================= Example =========================
//schema = {id: "number", name: "string", age: "number", married: "boolean"};
//DB = [];
//model("add", {id: 1, name: "Joe", age: "32", address: "Muntaner 262, Barcelona", married: "to Mary"})
//DB =====> [{id: 1, name: "Joe"}] => married and age not added because of the wrong type (Why?)

var schema = {id: "number", name: "string", age: "number", married: "boolean"};

function model(action, obj, schema){
    var DB = [];
    if(action == 'add'){
        var newObj = {};
        for(var key in schema){
            if(key in obj && typeof obj[key] == schema[key]){
                newObj[key] = obj[key];
            }
        }
        DB.push(newObj);
    } else {
        return "missing action argument or wrong provided";
    }
    return DB;
}

module.exports = {
    model
}