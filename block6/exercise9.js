//EXERCISE 9:
//Write a function called "sumAll" which takes one argument: an object.
//Sum all it's values. 
//If no object is provided, or the object is empty return 0.
//========================= Examples =========================
//var obj = {a: 1, b: 2, c: 2}
//sumAll(obj) =====> 5 
//sumAll({}) =====> 0 
//sumAll() =====> 0 

var obj = {a: 1, b: 2, c: 2};

function sumAll(obj){
    var sum = 0;
    if(obj == undefined || obj == null){
        return 0;
    } else {
        var arr = Object.values(obj); // Returns an array
        for(var i = 0; i < arr.length; i++){ // Standard loop to iterate through array numbers
            sum += arr[i]; // Total up the array with sum
        } 
    }
    return sum;
}

module.exports = {
    sumAll
}