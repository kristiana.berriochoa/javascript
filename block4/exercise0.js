//EXERCISE 0:
//Create a function called "isTrue".
//It should check if the data passed is NOT: undefined, an empty string, false, null, or 0.
//It takes one argument and returns true if it is NOT one of the falsy above, and false if it is.

var ele = [4, 'happy', 2, true, 0];
var ele = 0;
var ele = '';

function isTrue(ele){
    if(ele){
        return true;
    } else {
        return false;
    }
}

module.exports = {
    isTrue
}