//EXERCISE 4:
//Create a function called "checker".
//Loop through a string and check how many occurrences of commas and question marks there are.
//Return a string with the number of commas and question marks.
//Please make sure to use the appropriate grammatical form: 
//a) Use the plural "commas/question marks" for more than one
//b) Otherwise use the singular "comma/question mark"
//========================= Example =========================
//var str = 'hello, how are you today? I am not bad and you?' =====> 1 comma, 2 question marks

var str = 'hello, how are you today? I am not bad and you?';

function checker(str){
    var comma = '';
    var mark = '';
    var count_comma = 0;
    var count_mark = 0;
    for(var i = 0; i < str.length; i++){
        if(str.charAt(i) == ','){
            count_comma++;
        }
        if(str.charAt(i) == '?'){
            count_mark++;
        }
    }
    if(count_comma > 1){
        comma = (`${count_comma} commas`)
    } else {
        comma = (`${count_comma} comma`)
    }
    if(count_mark > 1){
        mark = (`${count_mark} question marks`)
    } else {
        mark = (`${count_mark} question mark`)
    }
    return comma + ',' + ' ' + mark;
}

module.exports = {
    checker
}