//EXERCISE 3:
//Write a function called "check_types", which takes an array as an argument.
//Loop through the array and check the "typeof" each element.
//Find out how many different data types there are.
//Return the number of data types.
//========================= Examples =========================
//check_types([12, 12, 12, [], [] ]) =====> returns 2; we have 2 data types (numbers and arrays)
//check_types([12, 45, 66, 88]) =====> returns 1; we have only one data type (numbers)

var arr = [12, 12, 12, [], []];

function check_types(arr){
	var arr2 = [];
    arr.forEach(element => {
		arr2[typeof(element)] = true;	
	});
	return Object.keys(arr2).length;
}

module.exports ={
    check_types
}