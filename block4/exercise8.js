//EXERCISE 8:
//Create a function called "uniqueElements", which takes an array as an argument.
//Create an empty array for the passed elements.
//Loop through the original array to get rid of duplicates.
//Return a string containing: 
//a) An array with the unique elements, without duplicates
//b) The original array left untouched.
//========================= Example =========================
//var originalArray = ['mike', 'jason', 'peter', 'robert', 'mike', 'jason', 'jenny', 'jane'];
//Expected output =====>
//a) old array: ['mike', 'jason', 'peter', 'robert', 'mike', 'jason', 'jenny', 'jane']
//b) new array: ['mike', 'jason', 'peter', 'robert', 'jenny', 'jane']

var arr = ['mike', 'jason', 'peter', 'robert', 'mike', 'jason', 'jenny', 'jane'];

function uniqueElements(arr){
	var arr2 = [];
	for(var i = 0; i < arr.length; i++){
	    if(!arr2.includes(arr[i])){
	    	arr2.push(arr[i]);
		}
	}
    return `old array ${arr}, new array ${arr2}`;
}

module.exports = {
    uniqueElements
}