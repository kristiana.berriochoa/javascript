//EXERCISE 10: Guess My Number
//Try to build a "Guess My Number" game with JavaScript
//================================================== 
//In this game, a computer randomly guesses a number and a player has to guess with a limited number of guesses. 
//================================================== 
//Requirements:
//- Game should generate a random number in a given range (i.e. 1-100)
//- Player should be told the range of possible numbers, and how many guesses they have
//- Game should display a prompt with an input field for a number
//- After each guess the game should display:
//  1) Number is too big (if the user's number is greater than the guessed number)
//  2) Number is too small (if the user's number is smaller than the guessed number)
//  3) If the player guesses the number, then the game should give a message and render text/image confirming the win
//  4) If the player can't guess the number in the given amount of guesses, then the game stops and reveals the number
//==================================================
//Help:
//- Use built-in JavaScript methods to create a random number
//- Remember to define the number of guesses first
//- Use a loop to repeat prompts asking the user for a number
//- Use conditionals to check if the numbers match
//- Use the range 0-10; if you put 100, it will be 1-100
//==================================================

function GuessMyNumber(){
    alert('Guess a number between 0-10. You have 3 guesses.');
    var computerGuess = Math.floor(Math.random() * 10);
    var attemtps = 3;
    var userGuess = prompt("What's your guess?");
    for(var i = 1; i < attemtps; i++){
        debugger
        if(userGuess == computerGuess && i <= attemtps){
            alert('Great guess! You won the game!')
            break;
        };
        if(userGuess < computerGuess){
            userGuess = prompt("That number is too low. Guess again!");
        } else {
            userGuess = prompt("That number is too high. Guess again!");
        }
    }
    if(i == attemtps && userGuess != computerGuess){
        alert('Sorry, you lost. The number was ' + computerGuess + '!')
        break;
    }  
}