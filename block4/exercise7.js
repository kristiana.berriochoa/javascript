//EXERCISE 7:
//Write a function called "booleanChecker". 
//It takes two arguments: an array and a maxCapacity, which is a number.
//Create an empty array called "bool" inside the function.
//Loop through the array.
//Every time there's a boolean, push it to the "bool array" until reaching maxCapacity.
//If the maxCapacity argument is missing, it should default to unlimited.
//========================= Example =========================
//var arr  = ['mike', 'john', true, false, 12, true, false, false, true, false, true, false]
//Expected output =====> `{Amount of booleans} booleans were found {booleans}`

var arr  = ['mike', 'john', true, false, 12, true, false, false, true, false, true, false];
var maxCap = 6;

function booleanChecker(arr, maxCap){
    var count = 0;
    var bools = [];
    arr.forEach(element => { // See if the element is a boolean
        if(typeof(element) == "boolean") { // Yes, it's a boolean
            if(maxCap == undefined || count < maxCap){ // If maxCap is undefined or amount is < 6
                bools.push(element); // Push the booleans to the new array
                count++; // Count how many were pushed
            }
        } 
    })
    return count + " booleans were found " + bools;
}

module.exports = {
    booleanChecker
}

/*
    maxCap == undefined        ||        count < maxCap
    ===================================================
1)  maxCap = undefined, count = 0
    undefined == undefined     ||        0 < undefined
    true                       ||        false               = true

2)  maxCap = 5, count = 3
    5 == undefined             ||        3 < 5
    false                      ||        true                = true

3)  maxCap = 5, count = 6
    6 == undefined           ||        6 < 5
    false                    ||        false                 = false
*/