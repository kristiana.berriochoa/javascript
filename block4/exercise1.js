//EXERCISE 1:
//Write a function called "check_who_is_older".
//It takes four arguments: a name, an age, another name2, and another age2.
//Check who is older.
//Return a sentence saying: "{{name}} age {{age}} is older than {{name2}} age {{age2}}".
//Should they be the same age, return "they are the same age".

var name = "Kristiana";
var age = 34;
var name2 = "Julie";
var age2 = 35;

function check_who_is_older(name, age, name2, age2){
    if(age > age2){
		return (`${name} age ${age} is older than ${name2} age ${age2}`)
	} 
	if(age2 > age){
		return (`${name2} age ${age2} is older than ${name} age ${age}`)
    } else {
		return (`they are the same age.`)
	}
}

module.exports = {
	check_who_is_older
}