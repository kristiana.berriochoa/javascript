//EXERCISE 5:
//Write a function called "howManyCaps", which counts the capital letters in a word.
//Return a sentence saying how many letters are capitalized and how many there are in total.
//========================= Example =========================
//var str = 'Antonello Sanna Likes videoGames' =====> There are 4 capitals and these are A, S, L, G

var str = 'Antonello Sanna Likes videoGames';

function howManyCaps(str){
    var count = 0;
    var spaces = 0
    var caps = [];
    var final = '';
    var capsList = '';
    for(var i = 0; i < str.length; i++){
        if(str.charAt(i) == " "){
            spaces++;
        } else {
            if(str.charAt(i) == str.charAt(i).toUpperCase()){
                count++;
                caps.push(str.charAt(i));
            }
        }
    }
    if(count > 1){
        final = (`There are ${count} capitals and these are`)
    } else {
        final = (`There is ${count} capital and these are`)
    }
    for(var i = 0; i < caps.length; i++){
        if(i == (caps.length - 1)){
            capsList = capsList + caps[i] + ''
        } else {
            capsList = capsList + caps[i] + ','
        } 
    }
    return final + ' ' + capsList;    
}

module.exports = {
    howManyCaps
}