//EXERCISE 2:
//Write a function called "is_an_even_number", which takes an array as argument. 
//Loop through the array and check if the elements are numbers (or a string that can be converted). 
//Also, check if they're even.
//Keep track of the even numbers by increasing the value of a variable "count".
//Return count.
//========================= Examples ========================= 
//var arr = [1, 5, 9, 33, 65, 122, 66, ['banana']] =====> return 2
//var arr = ["100", 33, "Hello"] =====> return 1

function is_an_even_number(arr){
    var count = 0;
    arr.forEach(element => {
        if(Number.isInteger(element) && element % 2 === 0){
		    count++;
		} else {
		var converted = Number.parseInt(element);
		if(Number.isInteger(converted) && converted % 2 === 0)
			count++;
		}
    })
	return count;
}

module.exports = {
    is_an_even_number
}

// arr.forEach(element => {
// !isNaN(element) && element % 2 === 0 ? count++ : null;
// });
// return count;
// }