//EXERCISE 9:
//Extend the previous exercise by returning only the array of unique elements.
//Take a second argument: max.
//All items in the new array must be numbers and bigger than or equal to max. (No conversions)
//========================= Example =========================
//var arr = [10, 44, 55, 66, 77, 55, 44, 3, 3, 3, 4, 5, 6, 54, "antonello", "33", "£", "66"]
//Expected output =====> [10, 44, 55, 66, 77, 54]

var arr = [10, 44, 55, 66, 77, 55, 44, 3, 3, 3, 4, 5, 6, 54, "antonello", "33", "£", "66"];
var max = 0;

function uniqueElements(arr, max){
	var arr2 = [];
	for(var i = 0; i < arr.length; i++){
		if(!arr2.includes(arr[i]) && typeof(arr[i]) == 'number' && arr[i] >= max){
	    	arr2.push(arr[i]);
		}
	}
    return `old array ${arr} \n new array ${arr2}`
}

module.exports = {
    uniqueElements
}