//************************************* BLOCK 4 ******************************************

//EXERCISE 0:
//Create a function called "isTrue".
//It should check if the data passed is NOT: undefined, an empty string, false, null, or 0.
//It takes one argument and returns true if it is NOT one of the falsy above, and false if it is.


//EXERCISE 1:
//Write a function called "check_who_is_older".
//It takes four arguments: a name, an age, another name2, and another age2.
//Check who is older.
//Return a sentence saying: "{{name}} age {{age}} is older than {{name2}} age {{age2}}".
//Should they be the same age, return "they are the same age".


//EXERCISE 2:
//Write a function called "is_an_even_number", which takes an array as argument. 
//Loop through the array and check if the elements are numbers (or a string that can be converted). 
//Also, check if they're even.
//Keep track of the even numbers by increasing the value of a variable "count".
//Return count.
//========================= Examples ========================= 
//var arr = [1, 5, 9, 33, 65, 122, 66, ['banana']] =====> return 2
//var arr = ["100", 33, "Hello"] =====> return 1


//EXERCISE 3:
//Write a function called "check_types", which takes an array as an argument.
//Loop through the array and check the "typeof" each element.
//Find out how many different data types there are.
//Return the number of data types.
//========================= Examples =========================
//check_types([12, 12, 12, [], [] ]) =====> returns 2; we have 2 data types (numbers and arrays)
//check_types([12, 45, 66, 88]) =====> returns 1; we have only one data type (numbers)


//EXERCISE 4:
//Create a function called "checker".
//Loop through a string and check how many occurrences of commas and question marks there are.
//Return a string with the number of commas and question marks.
//Please make sure to use the appropriate grammatical form: 
//a) Use the plural "commas/question marks" for more than one
//b) Otherwise use the singular "comma/question mark"
//========================= Example =========================
//var str = 'hello, how are you today? I am not bad and you?' =====> 1 comma, 2 question marks


//EXERCISE 5:
//Write a function called "howManyCaps", which counts the capital letters in a word.
//Return a sentence saying how many letters are capitalized and how many there are in total.
//========================= Example =========================
//var str = 'Antonello Sanna Likes videoGames' =====> There are 4 capitals and these are A, S, L, G


//EXERCISE 6:
//Write a function called "numberConverter", which takes an array as an argument. 
//Loop through the array and check if each element can be converted to a number. 
//Pay attention that empty arrays and empty strings:
//They are implicitly converted to 0, so you should exclude them; treat them as unconvertible.
//Any numbers inside strings or single items of the array should be converted.

//If convertible: convert them and keep track of how many can't be converted. 
//Return a string which outputs the result, as shown below:
//"[...numbers] were converted while NUMBER_OF_UNCONVERTABLE couldn't be converted".
//If the argument passed is already a number, it should be ignored. 
//If all arguments are numbers then the function should instead return the following:
//"No need for conversion"
//========================= Example =========================
//numberConverter([1,2,3,'4','5', {}, 33]) =====> "2 were converted to numbers: 4, 5, 1 couldn't be converted"


//EXERCISE 7:
//Write a function called "booleanChecker". 
//It takes two arguments: an array and a maxCapacity, which is a number.
//Create an empty array called "bool" inside the function.
//Loop through the array.
//Every time there's a boolean, push it to the "bool array" until reaching maxCapacity.
//If the maxCapacity argument is missing, it should default to unlimited.
//========================= Example =========================
//var arr  = ['mike', 'john', true, false, 12, true, false, false, true, false, true, false]
//Expected output =====> `{Amount of booleans} booleans were found {booleans}`


//EXERCISE 8:
//Create a function called "uniqueElements", which takes an array as an argument.
//Create an empty array for the passed elements.
//Loop through the original array to get rid of duplicates.
//Return a string containing: 
//a) An array with the unique elements, without duplicates
//b) The original array left untouched.
//========================= Example =========================
//var originalArray = ['mike', 'jason', 'peter', 'robert', 'mike', 'jason', 'jenny', 'jane'];
//Expected output =====>
//a) old array: ['mike', 'jason', 'peter', 'robert', 'mike', 'jason', 'jenny', 'jane']
//b) new array: ['mike', 'jason', 'peter', 'robert', 'jenny', 'jane']


//EXERCISE 9:
//Extend the previous exercise by returning only the array of unique elements.
//Take a second argument: max.
//All items in the new array must be numbers and bigger than or equal to max. (No conversions)
//========================= Example =========================
//var arr = [10, 44, 55, 66, 77, 55, 44, 3, 3, 3, 4, 5, 6, 54, "antonello", "33", "£", "66"]
//Expected output =====> [10, 44, 55, 66, 77, 54]


//EXERCISE 10: Guess My Number
//Try to build a "Guess My Number" game with JavaScript
//================================================== 
//In this game, a computer randomly guesses a number and a player has to guess with a limited number of guesses. 
//================================================== 
//Requirements:
//- Game should generate a random number in a given range (i.e. 1-100)
//- Player should be told the range of possible numbers, and how many guesses they have
//- Game should display a prompt with an input field for a number
//- After each guess the game should display:
//  1) Number is too big (if the user's number is greater than the guessed number)
//  2) Number is too small (if the user's number is smaller than the guessed number)
//  3) If the player guesses the number, then the game should give a message and render text/image confirming the win
//  4) If the player can't guess the number in the given amount of guesses, then the game stops and reveals the number
//==================================================
//Help:
//- Use built-in JavaScript methods to create a random number
//- Remember to define the number of guesses first
//- Use a loop to repeat prompts asking the user for a number
//- Use conditionals to check if the numbers match
//- Use the range 0-10; if you put 100, it will be 1-100
//==================================================