//EXERCISE 6:
//Write a function called "numberConverter", which takes an array as an argument. 
//Loop through the array and check if each element can be converted to a number. 
//Pay attention that empty arrays and empty strings:
//They are implicitly converted to 0, so you should exclude them; treat them as unconvertable.
//Any numbers inside strings or single items of the array should be converted.

//If convertable: convert them and keep track of how many can't be converted. 
//Return a string which outputs the result, as shown below:
//"[...numbers] were converted while NUMBER_OF_UNCONVERTABLE couldn't be converted".
//If the argument passed is already a number, it should be ignored. 
//If all arguments are numbers then the function should instead return the following:
//"No need for conversion"
//========================= Example =========================
//numberConverter([1, 2, 3, '4', '5', {}, 33]) =====> "2 were converted to numbers: 4, 5, 1 couldn't be converted"

var arr = [1, 2, 3, '4', '5', {}, 33];

function numberConverter(arr){
    var count = 0;
    var converted_arr = [];
    var unconverted = 0;
    console.log(arr); // Look at test arr running in terminal
    arr.forEach(element => {
        if(Number.isInteger(element)){  // Check if the element is a number
            count++; // Yes it is, add to count
        } else { // No it isn't, try to convert it
            var converted;
            if(typeof(element) === 'object' && element.length > 1){ // If array has > 2 elements
                converted = NaN; // Make the element a NaN; add to unconvertable
            } else {
                converted = Number.parseInt(element); // Otherwise, make the element an integer
            }
            if(Number.isNaN(converted)){ // If the element is not a number
                unconverted++; // Ignore and add to unconvertable
            } else {
                converted_arr.push(element); // Otherwise if actual number, push to new array
            }
        }
    })
    if(count == arr.length){
        return "no need for conversion";
    }
    return `${converted_arr.length} were converted to numbers: ${converted_arr}, ${unconverted} couldn't be converted`;
}

module.exports ={
    numberConverter
}