//EXERCISE 1: matchThemUp
//Create a function called "matchThemUp".
//It takes two arguments: the name of a male and the name of a female.
//It will ask both the below questions.
//If they have at least 50% of answers in common it will alert: "we have a match".
//Otherwise it will alert: "no match found!"
//=========================
//All questions must be answered with a 'yes' or 'no'.
//Use prompt to ask questions unless you feel like building a UI.
//End every question with the name of the person you are asking the question to.
//========================= Example ========================= 
//"Do you like going to the movies, Mike?"
//========================= Questions ========================= 
//1) Do you like pets? 
//2) Do you like beer? 
//3) Do you like reading books?
//4) Do you enjoy riding a bike? 
//5) Do you like mainstream music the most?

var arr = ['do you like pets', 'do you like beer', 'do you like reading books', 'do you enjoy riding a bike', 'do you like mainstream music the most'];

function matchThemUp(femaleName, maleName){
    debugger
    var name1 = femaleName;
    var name2 = maleName;
    var femaleYes = 0;
    var femaleNo = 0;
    var maleYes = 0;
    var maleNo = 0;
    arr.forEach(function(element){
        var femResult = prompt(element + ' ' + name1 + '?')
        if(femResult.toLowerCase() == "yes"){
            femaleYes++;
            return femaleYes;
        } else {
            femaleNo++;
            return femaleNo;
        }
    })
    arr.forEach(function(element){
        var maleResult = prompt(element + ' ' + name2 + '?')
        if(maleResult.toLowerCase() == "yes"){
            maleYes++;
            return maleYes;
        } else {
            maleNo++;
            return maleNo;
        } 
    })
    if(femaleYes >= 3 && maleYes >= 3 || femaleNo >= 3 && maleNo >= 3){
        alert("We have a match!");
    } else {
        alert("No match found!");
    }
}
