//EXERCISE 4:
//Extend "tally" so that now it can take two arguments.
//If the second argument is missing, it will default to 'obj'.
//If the second item is anything but 'arr', it will default to 'obj'.
//If the second argument is 'arr', instead return an array.
//The first element is the number, and the second is how many times it repeats in the array.
//========================= Examples ========================= 
//tally([2, 3, 4, 5, 5, 5, 5, 5, 5, 5, 6, 7, 6, 7, 6, 7, 5, 4, 3, 4, 5, 5, 6], 'arr')
//=====> [["2", 1], ["3", 2], ["4", 3], ["5", 10], ["6", 4], ["7", 3]]
//tally([2, 3, 4, 5, 5, 5, 5, 5, 5, 5, 6, 7, 6, 7, 6, 7, 5, 4, 3, 4, 5, 5, 6], 'obj')
//=====> {2: 1, 3: 2, 4: 3, 5: 10, 6: 4, 7: 3}

var arr = [2, 3, 4, 5, 5, 5, 5, 5, 5, 5, 6, 7, 6, 7, 6, 7, 5, 4, 3, 4, 5, 5, 6];

function tally(arr, action){
    var obj = {};
    var arr2 = [];
    arr.forEach(function(element){
        if(element in obj){
            obj[element] += 1;
        } else {
            obj[element] = 1;
        }
    })
    if(action == undefined || action != 'arr'){
        return obj;
    } else {
        if(action == 'arr'){
            for(var key in obj){
                arr2.push([key, obj[key]])
            }   
        }
    }
    return arr2;
}

module.exports = {
    tally
}