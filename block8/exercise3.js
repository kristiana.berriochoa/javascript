//EXERCISE 3:
//Write a function called "tally".
//Select all the unique elements of an array and count how many times the element is repeated.
//Create an object with two elements: the first is the number, and the second is number of repeats.
//========================= Example ========================= 
//tally([2, 3, 4, 5, 5, 5, 5, 5, 5, 5, 6, 7, 6, 7, 6, 7, 5, 4, 3, 4, 5, 5, 6])
//=====> {2: 1, 3: 2, 4: 3, 5: 10, 6: 4, 7: 3}

var arr = [2, 3, 4, 5, 5, 5, 5, 5, 5, 5, 6, 7, 6, 7, 6, 7, 5, 4, 3, 4, 5, 5, 6];

function tally(arr){
    var obj = {};
    arr.forEach(function(element){
        if(element in obj){
            obj[element] += 1;
        } else {
            obj[element] = 1;
        }
    })
    return obj;
}

module.exports = {
    tally
}