//************************************* BLOCK 8 ******************************************

//EXERCISE 1: matchThemUp
//Create a function called "matchThemUp".
//It takes two arguments: the name of a male and the name of a female.
//It will ask both the below questions.
//If they have at least 50% of answers in common it will alert: "we have a match".
//Otherwise it will alert: "no match found!"
//=========================
//All questions must be answered with a 'yes' or 'no'.
//Use prompt to ask questions unless you feel like building a UI.
//End every question with the name of the person you are asking the question to.
//========================= Example ========================= 
//"Do you like going to the movies, Mike?"
//========================= Questions ========================= 
//1) Do you like pets? 
//2) Do you like beer? 
//3) Do you like reading books?
//4) Do you enjoy riding a bike? 
//5) Do you like mainstream music the most? 


//EXERCISE 2:
//Write a function called "recursive".
//Loop (using recursion) through an array of objects (name, age).
//Take each pair and push them to an empty array, as nested arrays. 
//========================= Example ========================= 
//var arr = [{name: 'mike', age: 22}, {name: 'robert', age: 12}, {name: 'roger', age: 44}, {name: 'peter', age: 28}, {name: 'Ralph', age: 67}] 
//var arr2 = []
//recursive (...your arguments)
//=====> [[mike, 22], [robert, 12], [roger, 44], [peter, 28], [ralph, 67]]


//EXERCISE 3:
//Write a function called "tally".
//Select all the unique elements of an array and count how many times the element is repeated.
//Create an object with two elements: the first is the number, and the second is number of repeats.
//========================= Example ========================= 
//tally([2, 3, 4, 5, 5, 5, 5, 5, 5, 5, 6, 7, 6, 7, 6, 7, 5, 4, 3, 4, 5, 5, 6])
//=====> {2: 1, 3: 2, 4: 3, 5: 10, 6: 4, 7: 3}


//EXERCISE 4:
//Extend "tally" so that now it can take two arguments.
//If the second argument is missing, it will default to 'obj'.
//If the second item is anything but 'arr', it will default to 'obj'.
//If the second argument is 'arr', instead return an array.
//The first element is the number, and the second is how many times it repeats in the array.
//========================= Examples ========================= 
//tally([2, 3, 4, 5, 5, 5, 5, 5, 5, 5, 6, 7, 6, 7, 6, 7, 5, 4, 3, 4, 5, 5, 6], 'arr')
//=====> [["2", 1], ["3", 2], ["4", 3], ["5", 10], ["6", 4], ["7", 3]]
//tally([2, 3, 4, 5, 5, 5, 5, 5, 5, 5, 6, 7, 6, 7, 6, 7, 5, 4, 3, 4, 5, 5, 6], 'obj')
//=====> {2: 1, 3: 2, 4: 3, 5: 10, 6: 4, 7: 3}


//EXERCISE 5:
//Write your own sorting function to sort an array of numbers.
//This function can take up to two arguments: the first is the array to sort, and the second is the order of sorting.
//If the second argument isn't passed, it should default to 'ascending'.
//If the second argument passes and is 'descending', sort the array in descending order.
//If the second argument passes but isn't 'ascending' nor 'descending', return an error message.
//It should alert the user that he/she has passed the wrong data.
//=========================
//Don't use any built-in methods apart from 'console.log', 'alert', and 'document.write'.
//Try splitting this task into smaller inner functions to avoid repetition and gigantic unreadable if statements. 
//========================= Examples ========================= 
//sorter([11111, 9, 10, 12, 3, 321]) =====> (6) [3, 9, 10, 12, 321, 11111]
//sorter([11111, 9, 10, 12, 3, 321], 'ascending') =====> (6) [3, 9, 10, 12, 321, 11111]
//sorter([11111, 9, 10, 12, 3, 321], 'descending') =====> (6) [11111, 321, 12, 10, 9, 3]
//sorter([11111, 9, 10, 12, 3, 321], 'whatever') 
//=====> 'wrong order provided 'whatever', please provide us with ascending or descending order instructions'
 

//EXERCISE 6:
//Create a server function called "Server" which creates bank accounts at will. 
//Add, delete, update (name and amount), withdraw and deposit, as well as sort (by name and amount) accounts.
//Make sure that an account with the same name cannot be added more than once.
//All operations to delete, update, withdraw, and deposit should be done using a numeric 'id'. 
//Generate the 'id' and make sure it is unique.
//Make sure that with delete the 'id' doesn't shift if one item is removed.
//ONLY the Server constructor function should be defined; a new instance of it will be called below:
//========================= Example ========================= 
//var server = new Server()
//========================================= API to follow: =========================================
//server.app.post('accounts/new/:name/:amount')
//server.app.get('accounts/sort/:by/:order')
//server.app.get ('accounts/find')
//server.app.get ('accounts/find/:id')
//server.app.post('accounts/withdraw/:id/:amount')
//server.app.post('accounts/deposit/:id/:amount')
//server.app.post('accounts/delete/:id')
//server.app.post('accounts/update/:id/:amount/:name')
//========================= 
//For 'sorting', the expected values are:
//"Order" =====> 'asc' and 'desc' (which you can hopefully guess what they mean...)
//"By" =====> 'amount' and 'name'
//========================= 
//If the page is not found, return the following message: '404 page not found'.
//If one tries adding the same account twice, return: 'Account {ACCOUNT NAME} already present in db'.


//Challenge:
//Create a calculator with a graphical interface similar to that shown in the picture below, (or better).
//This calculator needs to work with the basic operations (add, subtract, multiply and divide). 
//Add a clear button to clear the display on click. 
//For the layout, you may consider using the bootstrap grid system.
//========================= Example ========================= 
//https://d2mxuefqeaa7sj.cloudfront.net/s_2DBAB7755F46B144CA2C8E6AE35D3FCDF36D0DB9BBEEEBF1654201DF0011FFDB_1509884234526_image.png
//*** Your solution goes to UI_Calculator folder ***