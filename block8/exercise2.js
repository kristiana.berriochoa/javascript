//EXERCISE 2:
//Write a function called "recursive".
//Loop (using recursion) through an array of objects (name, age).
//Take each pair and push them to an empty array, as nested arrays. 
//========================= Example ========================= 
//var arr = [{name: 'mike', age: 22}, {name: 'robert', age: 12}, {name: 'roger', age: 44}, {name: 'peter', age: 28}, {name: 'Ralph', age: 67}] 
//var arr2 = []
//recursive (...your arguments)
//=====> [[mike, 22], [robert, 12], [roger, 44], [peter, 28], [ralph, 67]]

var arr = [{name: 'robert', age: 22}, {name: 'robert', age: 12}, {name: 'roger', age: 44}, {name: 'peter', age: 98}, {name: 'Ralph', age: 67}];
var arr2 = [];
var tempArr = [];

function recursive(arr){
	for(var i = 0; i < arr.length; i++) {
	    tempArr.push(arr[i]["name"], arr[i]["age"]);
	};
	var counter = arr.length;
	while(counter > 0){
        arr2.push([tempArr[0], tempArr[1]]);
        tempArr.splice(0, 2);
	    counter--;
	}	
    return arr2;
}

module.exports = {
    recursive
}

/*
function recursive(arr, num){
    if(num < arr.length){
        if(!arr2.includes(arr[num].name)){
            arr2.push(); 
            num++;
            recursive(arr, num);
        }    
    } else {
        return;
    }
    return arr2;
}
*/