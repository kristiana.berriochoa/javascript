//EXERCISE 6:
//Create a server function called "Server" which creates bank accounts at will. 
//Add, delete, update (name and amount), withdraw and deposit, as well as sort (by name and amount) accounts.
//Make sure that an account with the same name cannot be added more than once.
//All operations to delete, update, withdraw, and deposit should be done using a numeric 'id'. 
//Generate the 'id' and make sure it is unique.
//Make sure that with delete the 'id' doesn't shift if one item is removed.
//ONLY the Server constructor function should be defined; a new instance of it will be called below:
//========================= Example ========================= 
//var server = new Server()
//========================================= API to follow: =========================================
//server.app.post('accounts/new/:name/:amount')
//server.app.get('accounts/sort/:by/:order')
//server.app.get ('accounts/find')
//server.app.get ('accounts/find/:id')
//server.app.post('accounts/withdraw/:id/:amount')
//server.app.post('accounts/deposit/:id/:amount')
//server.app.post('accounts/delete/:id')
//server.app.post('accounts/update/:id/:amount/:name')
//========================= 
//For 'sorting', the expected values are:
//"Order" =====> 'asc' and 'desc' (which you can hopefully guess what they mean...)
//"By" =====> 'amount' and 'name'
//========================= 
//If the page is not found, return the following message: '404 page not found'.
//If one tries adding the same account twice, return: 'Account {ACCOUNT NAME} already present in db'.

class Server {
    constructor(arg){
        let self = this;
        this.arg = 100;
        if(arg == undefined){ 
            arg = -1;
        };
        this.accounts = [];
        this.app = {  
            get: function(params){
                var arr = params.split('/');
                var action = arr[1];
                var firstArgGet = arr[2];
                var secondArgGet = arr[3];
                if(action == 'find'){
                    return self.actions.find(firstArgGet);
                }
                if(action == 'sort'){
                    return self.actions.sort(firstArgGet, secondArgGet);
                } else {
                    return `404 page not found`;
                }
            },
            post: function(params){
                var arr = params.split('/');
                var action = arr[1];
                var firstArg = arr[2];
                var secondArg = Number(arr[3]); //check how ternary operator works
                var newName = arr[4];
                if(action == 'new'){
                    return self.actions.new(firstArg, secondArg);
                };
                if(action == 'delete'){
                    return self.actions.delete(firstArg);
                };
                if(action == 'update'){
                    return self.actions.update(firstArg, secondArg, newName);
                };
                if(action == 'withdraw'){
                    return self.actions.withdraw(firstArg, secondArg);
                };
                if(action == 'deposit'){
                    return self.actions.deposit(firstArg, secondArg);
                } else {
                    return `404 page not found`;
                }; 
            }
        }
        this.actions = {
            new: function(name, amount){
            var index = self.accounts.findIndex(item => item.name == name);
                if(index === -1){
                    var obj = {};
                        obj['name'] = name;
                        obj['amount'] = amount;
                        obj['id'] = self.arg++;
                    self.accounts.push(obj);
                    return obj;
                } else {
                    return `Account ${self.accounts[index].name} already present in db`;
                }
            },
            delete: function(id){
            var index = self.accounts.findIndex(item => item.id == id);
                if(index != -1){
                    var result = self.accounts.splice(index, 1);
                    return result;
                } else {
                    return `Account ${self.accounts[index].name} not present in db`;
                }
            },
            update: function(id,amount,newName){
            var index = self.accounts.findIndex(item => item.id == id);
                if(index != -1){
                    self.accounts[index].name = newName;
                    self.accounts[index].amount = amount;
                    return self.accounts[index];
                } else {
                    return `Account ${self.accounts[index].name} not present in db`;
                }
            },
            withdraw: function(id, amount){
            var index = self.accounts.findIndex(item => item.id == id);
                if(index != -1){
                    var result = self.accounts[index].amount -= amount;
                    return `Amount should be ${result} after withdraw`;
                } else {
                    return `Account ${self.accounts[index].name} not present in db`;
                }
            },
            deposit: function(id, amount){
            var index = self.accounts.findIndex(item => item.id == id);
                if(index != -1){
                    var result = self.accounts[index].amount += amount;
                    return `Amount should be ${result} after deposit`;
                } else {
                    return `Account ${self.accounts[index].name} not present in db`;
                }
            },
            find: function(id){
                if(id == undefined){
                    return self.accounts;
                } else {
                    var result = self.accounts.filter(account => account.id == id);
                    return result;
                }     
            },
            sort: function(by, order){
                if(order === 'asc' && by === 'name'){
                    self.accounts.sort(function(a, b){
                        if(a.name < b.name)
                        return -1;
                    }) 
                    return self.accounts; 
                };
                if(order === 'desc' && by === 'name'){
                    self.accounts.sort(function(a, b){
                        if(a.name < b.name)
                        return -1;
                    })
                    return self.accounts.reverse();
                };
                if(order === 'asc' && by === 'amount'){
                    self.accounts.sort(function(a, b){
                        return a.amount - b.amount;
                    })
                    return self.accounts;
                };
                if(order === 'desc' && by === 'amount'){
                    self.accounts.sort(function(a, b){
                        return b.amount - a.amount;
                    })
                    return self.accounts;
                };
            }  
        }
    }
}

module.exports = {
    Server
}

/*
let temp = self.accounts[index].name;
return `Account ${temp} has withdrawn ${amount}`
*/
