//EXERCISE 5:
//Write your own sorting function to sort an array of numbers.
//This function can take up to two arguments: the first is the array to sort, and the second is the order of sorting.
//If the second argument isn't passed, it should default to 'ascending'.
//If the second argument passes and is 'descending', sort the array in descending order.
//If the second argument passes but isn't 'ascending' nor 'descending', return an error message.
//It should alert the user that he/she has passed the wrong data.
//=========================
//Don't use any built-in methods apart from 'console.log', 'alert', and 'document.write'.
//Try splitting this task into smaller inner functions to avoid repetition and gigantic unreadable if statements. 
//========================= Examples ========================= 
//sorter([11111, 9, 10, 12, 3, 321]) =====> (6) [3, 9, 10, 12, 321, 11111]
//sorter([11111, 9, 10, 12, 3, 321], 'ascending') =====> (6) [3, 9, 10, 12, 321, 11111]
//sorter([11111, 9, 10, 12, 3, 321], 'descending') =====> (6) [11111, 321, 12, 10, 9, 3]
//sorter([11111, 9, 10, 12, 3, 321], 'whatever') 
//=====> 'wrong order provided 'whatever', please provide us with ascending or descending order instructions'

function sorter(arr, sortOrder){
    if(sortOrder == undefined){
        sortOrder = 'ascending'
    }
    if(sortOrder != 'ascending' && sortOrder != 'descending'){
        return `wrong order provided ${sortOrder} please provide us with ascending or descending order instructions`;
    }
    for(var x = 0; x < arr.length; x++){
        for(var y = x + 1; y < arr.length; y++){
            if(sortOrder == 'descending'){
                descending(arr);
            } else {
                ascending(arr);
            }
        }
    }
    function ascending(arr){
        if(arr[x] > arr[y]){
            var temp = arr[x];
            arr[x] = arr[y];
            arr[y] = temp;
        }
    }
    function descending(arr){
        if(arr[x] < arr[y]){
            var temp = arr[x];
            arr[x] = arr[y];
            arr[y] = temp;
        }
    }
    return arr;
}

module.exports = {
    sorter
}