//EXTRA CHALLENGE: Rock-paper-scissors
// Requirements:
// - The computer should choose randomly every turn
// - The player can click on a picture representing the option and choose it.
// - You should see if you won or lost at every turn
// - in order to win the match the player or the computer needs to win twice.
// - there need to be a score keeper to  show the current score of the player and the computer.
// - In case both select the same option then is a draw and no points should be given out.

// ----------

// ***Your solution goes to Rock_paper_scissors folder
// if you are going to add UI ***

function getHumanChoice(userInput) { 
  userInput = prompt("Let's play rock, paper, scissors! What is your choice?")
  if(userInput === 'rock' || userInput ==='paper' || userInput ==='scissors'){
    return userInput;
  } else {
    userInput === "forfeit"
    console.log('Sorry, not a valid choice');
  }
};

function getComputerChoice(){
  switch(Math.floor(Math.random() * 3)) {
    case 0:
      return 'rock';
    case 1:
      return 'scissors';
    case 2:
      return 'paper'
  }
};

function determineWinner(getHumanChoice,getComputerChoice){
  if(getHumanChoice === getComputerChoice) {
    return "It's a tie!";
  }
  if(getHumanChoice === 'rock'){
    if(getComputerChoice === 'paper'){
      return 'Computer wins!';
    } else {
      return 'You win!';
    }
  }
  if(getHumanChoice === 'paper'){
    if(getComputerChoice === 'scissors'){
        return 'Computer wins!';
    } else {
        return 'You win!'
    }
  }
  if(getHumanChoice === 'scissors'){
    if(getComputerChoice === 'rock'){
      return 'Computer wins!';  
    } else {
       return 'You win!'
    }
  }
};  
    
function playGame(rounds = 3, playerScore = 0, computerScore = 0){
  var humanChoice = getHumanChoice();
  var computerChoice = getComputerChoice();
  console.log('You threw: ' + humanChoice +' and '+ 'the computer threw: ' + computerChoice + '.');
  var results = determineWinner(humanChoice,computerChoice);
  if(results === "It's a tie!"){
    return playGame(rounds, playerScore, computerScore);
  }
  if(results === 'You win!'){
    playerScore += 1;
    if(playerScore >= Math.ceil(rounds/2)){
      return 'The player has won!';
    }
    else if(computerScore >= Math.ceil(rounds/2)){
      return 'The computer has won!';
    }
  return playGame(rounds, playerScore, computerScore);
  }
  else if(results === "Computer wins!"){
    computerScore += 1;
    if(computerScore >= Math.ceil(rounds/2)){
      return 'The computer has won!';
    }
  }
  return playGame(rounds,playerScore, computerScore)
};

playGame();

