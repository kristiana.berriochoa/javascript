//EXERCISE 2:
//To practice passing data between functions let's create a crypto converter…
//Create several functions: addCurrency, findcurrency, converter, tellConversion.
//Each function will be responsible for its own tasks, and to call the next function.
//Exclusively call 'addCurrency' and it will call the others.
//The function 'addCurrency' takes three arguments: a coin, its value (amount of coins), and an array of coins (coin database)
//The coin should be an object with the following structure:
/*{
    coin: 'coin_here', 
    rate: CONVERSION_RATE_TO_USD_TYPE_NUMBER
}*/
//========================= Example ========================= 
//addCurrency({coin: 'bitcoin', rate: 8000}, 2, crypt)
//=====> {coin: 'bitcoin', rate: 8000} is the coin, 2 is the value/amount, crypt is the name of the DB.
//First time you run it, return "New coin Bitcoin added to Database".
//Second time you run it, return "You will receive 16000 usd for your 2 bitcoins".
//=========================
//'addCurrency' should first check if the coin already exists in the DB.
//If it doesn't, add it and return the following: "New coin {YOUR_NEW_ADDED_COIN_GOES_HERE} added to Database"
//If the coin does exist in the DB, call 'findcurrency', which retrieves the conversion rate of the given currency.
//Passing that should call 'convert', which is in charge of doing the actual conversion.
//'tellConversion' returns the final message: "You will receive {AMOUNT} usd for your 2 {COINS_NAME}".
//Please make sure that when adding the new currency the output message capitalizes the coin name.

var crypt = [];
var obj =    
    {
        coin: 'bitcoin',
        rate: 8000
    };

var addCurrency = function(obj, amount, crypt){
    var index = crypt.findIndex(x => x.coin == obj.coin);
    if(index == -1){
        crypt.push(obj);
        return `New coin ${obj.coin.charAt(0).toUpperCase() + obj.coin.slice(1)} added to Database`;
    } else {
        return findcurrency(obj, amount, index, crypt);
    }
}
var findcurrency = function(obj, amount, index, crypt){
        var convertRate = crypt[index].rate;
        return converter(obj, amount, convertRate);
}
var converter = function(obj, amount, convertRate){
        var converted = convertRate * amount;
        return tellConversion(obj, amount, converted);
}
var tellConversion = function(obj, amount, converted){
        var plural = 's';
        return `You will receive ${converted} usd for your ${amount} ${obj.coin.concat(plural)}`;
}

module.exports = {
    addCurrency, findcurrency, converter, tellConversion
}