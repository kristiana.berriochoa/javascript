//EXERCISE 5:
//Without Googling how to shuffle elements inside an array in javascript, create a function called "shuffle".
//Implement in it your own algorithm to shuffle the elements inside a given array.
//========================= Examples ========================= 
//var arr = ['one', 'two', 'three', 'four']
//shuffle(arr) =====> (4) ["three", "one", "four", "two"]
//shuffle(arr) =====> (4) ["two", "one", "three", "four"]
//shuffle(arr) =====> (4) ["one", "two", "three", "four"]
//shuffle(arr) =====> (4) ["three", "two", "four", "one"]

var arr = ['one', 'two', 'three', 'four'];

function shuffle(arr){
    for(var i = arr.length - 1; i > 0; i--){
        var random = Math.floor(Math.random() * (i + 1));
        var temp = arr[i];
        arr[i] = arr[random];
        arr[random] = temp;
    }
    return arr;
}

module.exports = {
    shuffle
}