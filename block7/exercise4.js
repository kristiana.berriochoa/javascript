//EXERCISE 4:
//Implement a representation of the universe where matter and energy is conserved. 
//To do so, implement one object called "Universe" that contains two objects: matter and energy. 
//If matter is to be destroyed, call 'Universe.matter.destroy(5)', but the amount of energy in the universe needs to be increased.
//Call 'Universe.energy.total()' to obtain a total value of energy.
//This total has increased +5 compared to the energy value previous to calling 'Universe.matter.destroy(5)'. 
//Of course, the total amount of matter obtained by calling 'Universe.matter.total()' has reduced by 5 compared to its initial value.
//=========================
//Implement this object using context.
//The matter and energy objects are defined within an object called "Universe".
//No other variable should be defined outside the 'Universe' object.
//Implement the "create" methods for both 'matter' an 'energy', which are opposite their counterparts.
//Give an initial amount for either 'energy' or 'matter', otherwise it should default to 0.
//========================= Examples ========================= 
//var universe = new Universe(10, 'matter')
//Universe.matter.total =====> 10 
//Universe.energy.total =====> 0 
//========================= 
//Or with no initial amount: 
//var universe = new Universe()
//Universe.matter.total  0 
//Universe.energy.total =====> 0 
//Universe.matter.destroy(5) =====> 0 
//Universe.eatter.total =====> -5 
//Universe.energy.total =====> 5 
//Universe.energy.destroy(-5) =====> 0 
//Universe.matter.total =====> -10 
//Universe.energy.total =====> 10 
//Universe.energy.create(5) =====> 0 
//Universe.matter.total =====> -15 
//Universe.energy.total =====> 15 
//========================= Notes ========================= 
//Initially make your universe contain 0 matter and energy.
//Destroying a negative amount of energy or matter is equal to creating a positive amount of each (vice versa for creating them)

class Universe {
  constructor(value, destination){
    this.matter = {
      total: 0   
    };
    this.energy = {
      total: 0
    };
    let self = this;
    this.matter.destroy = function(amount){
      self.matter.total -= amount;
      self.energy.total += amount;
    }
    this.matter.create = function(amount){
      self.matter.total += amount;
      self.energy.total -= amount;
    }
    this.energy.destroy = function(amount){
      self.matter.total += amount;
      self.energy.total -= amount;
    }
    this.energy.create = function(amount){
      self.matter.total -= amount;
      self.energy.total += amount;
    }   
    if(value == undefined || Number.isNaN(value)){
      return;
    }
    if(destination == 'matter'){
      this.matter.total = value;
    }
    if(destination == 'energy'){
      this.energy.total = value;
    }
  }
}  

module.exports = {
    Universe
}