//EXERCISE 1:
//Write a function called "moviesDB", used to create a movie database.
//This function takes three arguments: an array (actual db), a genre, and an object (the movie).
//Check if the genre exists, and if it doesn't, add it on.
//Also, check if the movie exists, and add it on if it doesn't.
//See the example of the data structure you MUST follow.
//If the movie is already present it should not add it again.
//It should instead return the following message: 'the movie the < YOUR_MOVIE_GOES_HERE > is already in the database!'
//========================= Example ========================= 
/*DB = [
    {
        genre:'thriller', 
        movies:[
            {
                title: 'the usual suspects', release_date: 1999
            }
        ]
    },
    {
        genre:'commedy', 
        movies:[
            {
                title: 'pinapple express', release_date: 2008
            }
        ]
    }
]*/

var DB = [
    {
        genre:'thriller', 
        movies:[
            {
                title:'the usual suspects', release_date:1999
            }
        ]
    },
    {
        genre:'commedy', 
        movies:[
            {
                title:'pinapple express', release_date:2008
            }
        ]
    }
];

function moviesDB(DB, genre, obj){
    var index = DB.findIndex(x => x.genre == genre);
    if(index != -1){
        var indexMovie = DB.findIndex(x => x.movies[0].title == obj.title); 
        if(indexMovie != -1){
            return `the movie ${obj.title} is already in the database!`;
        } else {
            DB[index].movies.push(obj);
        }
    } else {
        var newObje2 = {};
        newObje2['genre'] = genre;
        newObje2['movies'] = [obj];
        DB.push(newObje2);
    }
    return DB;
}

module.exports = {
    moviesDB
}