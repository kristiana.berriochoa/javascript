//************************************* BLOCK 7 ******************************************

//EXERCISE 1:
//Write a function called "moviesDB", used to create a movie database.
//This function takes three arguments: an array (actual db), a genre, and an object (the movie).
//Check if the genre exists, and if it doesn't, add it on.
//Also, check if the movie exists, and add it on if it doesn't.
//See the example of the data structure you MUST follow.
//If the movie is already present it should not add it again.
//It should instead return the following message: 'the movie the < YOUR_MOVIE_GOES_HERE > is already in the database!'
//========================= Example ========================= 
/*DB = [
    {
        genre:'thriller', 
        movies:[
            {
                title: 'the usual suspects', release_date: 1999
            }
        ]
    },
    {
        genre:'commedy', 
        movies:[
            {
                title: 'pinapple express', release_date: 2008
            }
        ]
    }
]*/


//EXERCISE 2:
//To practice passing data between functions let's create a crypto converter…
//Create several functions: addCurrency, findcurrency, converter, tellConversion.
//Each function will be responsible for its own tasks, and to call the next function.
//Exclusively call 'addCurrency' and it will call the others.
//The function 'addCurrency' takes three arguments: a coin, its value (amount of coins), and an array of coins (coin database)
//The coin should be an object with the following structure:
/*{
    coin: 'coin_here', 
    rate: CONVERSION_RATE_TO_USD_TYPE_NUMBER
}*/
//========================= Example ========================= 
//addCurrency({coin: 'bitcoin', rate: 8000}, 2, crypt)
//=====> {coin: 'bitcoin', rate: 8000} is the coin, 2 is the value/amount, crypt is the name of the DB.
//First time you run it, return "New coin Bitcoin added to Database".
//Second time you run it, return "You will receive 16000 usd for your 2 bitcoins".
//=========================
//'addCurrency' should first check if the coin already exists in the DB.
//If it doesn't, add it and return the following: "New coin {YOUR_NEW_ADDED_COIN_GOES_HERE} added to Database"
//If the coin does exist in the DB, call 'findcurrency', which retrieves the conversion rate of the given currency.
//Passing that should call 'convert', which is in charge of doing the actual conversion.
//'tellConversion' returns the final message: "You will receive {AMOUNT} usd for your 2 {COINS_NAME}".
//Please make sure that when adding the new currency the output message capitalizes the coin name.


//EXERCISE 3:
//Create a constructor function called "bankAccount".
//It takes three methods: withdraw (reduces balance), deposit (adds to balance), and balance (returns account amount).
//All of these are functions, using an amount to increase/decrease, or display a balance.
//All of them are inside the 'bankAccount' class.
//The function works with or without an initial amount.
//========================= Example ========================= 
//var account = new bankAccount(10)
//account.withdraw(2)
//account.withdraw(5)
//account.deposit(4)
//account.deposit(1)
//account.balance() =====> 8 


//EXERCISE 4:
//Implement a representation of the universe where matter and energy is conserved. 
//To do so, implement one object called "Universe" that contains two objects: matter and energy. 
//If matter is to be destroyed, call 'Universe.matter.destroy(5)', but the amount of energy in the universe needs to be increased.
//Call 'Universe.energy.total()' to obtain a total value of energy.
//This total has increased +5 compared to the energy value previous to calling 'Universe.matter.destroy(5)'. 
//Of course, the total amount of matter obtained by calling 'Universe.matter.total()' has reduced by 5 compared to its initial value.
//=========================
//Implement this object using context.
//The matter and energy objects are defined within an object called "Universe".
//No other variable should be defined outside the 'Universe' object.
//Implement the "create" methods for both 'matter' an 'energy', which are opposite their counterparts.
//Give an initial amount for either 'energy' or 'matter', otherwise it should default to 0.
//========================= Examples ========================= 
//var universe = new Universe(10, 'matter')
//Universe.matter.total =====> 10 
//Universe.energy.total =====> 0 
//========================= 
//Or with no initial amount: 
//var universe = new Universe()
//Universe.matter.total  0 
//Universe.energy.total =====> 0 
//Universe.matter.destroy(5) =====> 0 
//Universe.eatter.total =====> -5 
//Universe.energy.total =====> 5 
//Universe.energy.destroy(-5) =====> 0 
//Universe.matter.total =====> -10 
//Universe.energy.total =====> 10 
//Universe.energy.create(5) =====> 0 
//Universe.matter.total =====> -15 
//Universe.energy.total =====> 15 
//========================= Notes ========================= 
//Initially make your universe contain 0 matter and energy.
//Destroying a negative amount of energy or matter is equal to creating a positive amount of each (vice versa for creating them)


//EXERCISE 5:
//Without Googling how to shuffle elements inside an array in javascript, create a function called "shuffle".
//Implement in it your own algorithm to shuffle the elements inside a given array.
//========================= Examples ========================= 
//var arr = ['one', 'two', 'three', 'four']
//shuffle(arr) =====> (4) ["three", "one", "four", "two"]
//shuffle(arr) =====> (4) ["two", "one", "three", "four"]
//shuffle(arr) =====> (4) ["one", "two", "three", "four"]
//shuffle(arr) =====> (4) ["three", "two", "four", "one"]