//EXERCISE 3:
//Create a constructor function called "bankAccount".
//It takes three methods: withdraw (reduces balance), deposit (adds to balance), and balance (returns account amount).
//All of these are functions, using an amount to increase/decrease, or display a balance.
//All of them are inside the 'bankAccount' class.
//The function works with or without an initial amount.
//========================= Example ========================= 
//var account = new bankAccount(10)
//account.withdraw(2)
//account.withdraw(5)
//account.deposit(4)
//account.deposit(1)
//account.balance() =====> 8  

class BankAccount {
    constructor(amount){
        if(amount == undefined){
            amount = 0;
        }
        this.total = amount;
    }
    deposit(value){
        return this.total += value;
    }
    withdraw(value){
        return this.total -= value;
    }
    balance(){
        return this.total;
    }
}

module.exports = {
    BankAccount
}