//EXERCISE 9:
//Define the name of a user. Define the year of birth of a user.
//Return the user's name and current age in a sentence like following:
//========================= Example =========================
//Expected output =====> Hello Jason, you are 34 years old

var name = 'Kristiana';
var birth_year = 1985;
var current_year = 2019;

var getAge  = function (birth_year, current_year, name){
    var age = (current_year - birth_year);
    return "Hello " + name + " you are " + age + " years old";   
}

module.exports = {
    name, birth_year, getAge
}