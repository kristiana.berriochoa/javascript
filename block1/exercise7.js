//EXERCISE 7:
//Create a function called "isEven" which takes an argument: "a".
//It then returns true if the argument passed is even, and false if it is odd.
//========================= Examples =========================
//isEven(7) =====> false
//isEven(2) =====> true

var isEven = function(a){
	return (a % 2 ==0);
}

module.exports = {
    isEven
}
