//EXERCISE 8:
//Knowing that the minimum age for driving a 50cc scooter is 15 define a variable called "age".
//Return true if you are old enough to drive a scooter, and false if you are not.

var age = 34;
var minAge = 15;

var checkAge = function (age, minAge){
    return age >= minAge;
}

module.exports = {
    checkAge,
    age,
    minAge
}