//EXERCISE 4:
//Ever wonder how much a tea you would need for a lifetime supply? Let's find out!!!
//Store your current age in a variable and your estimated end age in another variable. 
//Store how many teas you drink per day, on average. 
//Calculate how many you would need until the end! Output the result!

var age = 33;
var end_age = 94;
var teas_day = 2;

var howManyTeas = function(age, end_age, teas_day){
    var sum = (end_age - age) * (teas_day * 365);
    return sum; 
}

module.exports = {
    howManyTeas,
    age,
    end_age,
    teas_day
}