//EXERCISE 1:
//Create a variable called 'apple' and assign it a value of 5. 
//Then create another variable called 'apple2' and give it a value of 15.
//Add them together and return their sum.

var apple = 5, apple2 = 10;

function total(apple, apple2){
	var sum = apple + apple2;
    return sum;
}

module.exports = {
    apple, apple2, total
}