//************************************* BLOCK 1 ******************************************

//Please write your solutions in the corresponding folder/file to make them available for the testing.

//IN ORDER TO TEST YOUR SOLUTIONS, RUN THE FOLLOWING COMMANDS IN TERMINAL:
//First time:
//Just once from the folder with the exercises you cloned, ie BCS_JS_BOOTCAMP_FILES_TDD:
//npm install --- This will install needed npm packages for testing

//To test an exercise from the folder with exercises:
//npm run test-single -- test/'NAME_OF_THE_TEST_FILE_YOU_ARE_TESTING'

//========================= Examples =========================
//If you are doing exercise 1 of block 1 it would be: 
//npm run test-single -- test/b1exercise1.test.js

//Simply change block/exercises numbers; for Block 02 Exercise 3 run:
//npm run test-single -- test/b2exercise3.test.js


//************************************* EXERCISES ******************************************
//EXERCISE 1:
//Create a variable called 'apple' and assign it a value of 5. 
//Then create another variable called 'apple2' and give it a value of 15.
//Add them together and return their sum.


//EXERCISE 2:
//Create two variables: 'a' and 'b'. 
//Assign 10 to 'a' and 24 to 'b'.
//Multiply them and return the amount.


//EXERCISE 3: Age calculator
//Want to find out how old you'll be? Calculate it! 
//Store your birth year in a variable. Store a future year in a variable. Output the age!


//EXERCISE 4:
//Ever wonder how much a tea you would need for a lifetime supply? Let's find out!!!
//Store your current age in a variable and your estimated end age in another variable. 
//Store how many teas you drink per day, on average. 
//Calculate how many you would need until the end! Output the result!


//EXERCISE 5:
//Define a function called "compare" which takes two arguments.
//Return true if the first is bigger than the second, and false if it is not.
//========================= Examples =========================
//compare(10, 34) =====> false
//compare(100, 34) =====> true


//EXERCISE 6:
//Define a function called "compare" which takes two arguments:
//Return true if the first is equal to the second, and false if it is not (strict equality).
//========================= Examples =========================
//compare(10, 34) =====> false
//compare(100, 100) =====> true
//compare(100, '100') =====> false


//EXERCISE 7:
//Create a function called "isEven" which takes an argument: "a".
//It then returns true if the argument passed is even, and false if it is odd.
//========================= Examples =========================
//isEven(7) =====> false
//isEven(2) =====> true


//EXERCISE 8:
//Knowing that the minimum age for driving a 50cc scooter is 15 define a variable called "age".
//Return true if you are old enough to drive a scooter, and false if you are not.


//EXERCISE 9:
//Define the name of a user. Define the year of birth of a user.
//Return the user's name and current age in a sentence like following:
//========================= Example =========================
//Expected output =====> Hello Jason, you are 34 years old


//EXERCISE 10:
//Define the user's year of birth. Assign the current year to a variable.
//Return the following message, replacing the word *'DAYS'* with the actual value.
//========================= Example =========================
//Expected output =====> You have lived for *'DAYS'* days already!


//EXERCISE 11:
//It's hot out! Let's make a converter based on the steps here: http://www.mathsisfun.com/temperature-conversion.html

//Store a celsius temperature in a variable; convert it to fahrenheit and output the result.
//Now store a fahrenheit temperature in a variable; convert it to celsius and output the result.