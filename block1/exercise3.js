//EXERCISE 3: Age calculator
//Want to find out how old you'll be? Calculate it! 
//Store your birth year in a variable. Store a future year in a variable. Output the age!

var date_of_birth = 1985;
var future_year = 2020;

var ageCalc = function(date_of_birth, future_year){
	var sum = future_year - date_of_birth;
    return sum;
}

module.exports = {
    ageCalc,
    date_of_birth,
    future_year,
}