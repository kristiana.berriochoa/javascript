//EXERCISE 11:
//It's hot out! Let's make a converter based on the steps here: http://www.mathsisfun.com/temperature-conversion.html

//Store a celsius temperature in a variable; convert it to fahrenheit and output the result.
//Now store a fahrenheit temperature in a variable; convert it to celsius and output the result.

var celsius = 35;
var fahrenheit = 99;

var toCelsius = function (fahrenheit){
    return (fahrenheit - 32) * (5/9);
}

var toFahr = function (celsius){
    return ((celsius * 9) / 5) + 32;
}

module.exports = {
    celsius, 
    fahrenheit, 
    toCelsius, 
    toFahr,
}