//EXERCISE 6:
//Define a function called "compare" which takes two arguments:
//Return true if the first is equal to the second, and false if it is not (strict equality).
//========================= Examples =========================
//compare(10, 34) =====> false
//compare(100, 100) =====> true
//compare(100, '100') =====> false

var compare = function(a, b){
	return a === b;
}

module.exports = {
    compare
}
