//EXERCISE 10:
//Define the user's year of birth. Assign the current year to a variable.
//Return the following message, replacing the word *'DAYS'* with the actual value.
//========================= Example =========================
//Expected output =====> You have lived for *'DAYS'* days already!

var dob = 1985;
var now = 2019;

var howManyDays = function (dob, now){
    var age = (now - dob) * 365;
    return "you have lived for" + " " + age + " " + "days already!";
}
module.exports = {
    now, dob, howManyDays
}