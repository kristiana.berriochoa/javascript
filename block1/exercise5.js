//EXERCISE 5:
//Define a function called "compare" which takes two arguments.
//Return true if the first is bigger than the second, and false if it is not.
//========================= Examples =========================
//compare(10, 34) =====> false
//compare(100, 34) =====> true

var compare = function(a, b){
	return a > b;
}

module.exports = {
    compare
}
