//EXERCISE 2:
//Create two variables: 'a' and 'b'.
//Assign 10 to 'a' and 24 to 'b'. 
//Multiply them and return the amount.

var a = 10;
var b = 24;

function multy(a, b){
	var sum = a * b; 
    return sum;
}

module.exports = {
    a, b, multy
}
