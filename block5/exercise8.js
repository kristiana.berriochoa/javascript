//EXERCISE 8:
//Write a function called "strCut".
//It takes three arguments: a string and two indexes.
//Return a string after removing the letters of the two given indexes.
//========================= Examples ========================= 
//strCut('Antonello', 0,8) =====> "ntonell"
//strCut('Antonello', 3,5) =====> "Antnllo"
//strCut('Antonello', 2,8) =====> "Anonell"

function strCut(word, start, end){
    var result = '';
    for(var i = 0; i < word.length; i++){
        if(i != start && i != end){ // if it doesn't equal
            result += word[i]; 
        } 
    }
    return result;
}

module.exports = {
    strCut
}