//EXERCISE 4:
//Write a function called "tellAge" that tells you how old you are, based on the date-of-birth passed.
//This function will take three arguments: a month, a day, and a year.
//You should use the "Date" object to set the value of today.
//If the birth-date is less than one year from the current date, return: "you are NUMBER_DAYS old"
//If it's more than one year, return: "you are NUMBER_YEARS old"
//Date object: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date
//========================= Examples ========================= 
//Assuming today is April 11, 2018:
//tellAge(8, 2, 1982) =====> you are 35 years old
//tellAge(4, 3, 1982) =====> you are 36 years old
//tellAge(4, 9, 2018) =====> you are 2 days old

function tellAge(month, day, year){
    var today = Date.now(); // From 1970 it's 49 years (Date.now is a number)
    var birthday = new Date(year, month-1, day); // For year 2000, 30 years (new Date is an object)
    var differenceInMillis = today - birthday.getTime(); // Add getTime to make birthday a number
    
    var differenceInDays = Math.floor(differenceInMillis / 1000 / 60 / 60 / 24);
    var differenceInYears = Math.floor(differenceInDays / 365);

    var plural;
    if(differenceInYears < 1){
        if(differenceInDays == 1){
            plural = "day";
        } else {
            plural = "days";
        }
        return `You are ${differenceInDays} ${plural} old`;
    } else {
        if(differenceInYears == 1){
            plural = "year";
        } else {
            plural = "years";
        }
        return `You are ${differenceInYears} ${plural} old`;
    }
}

module.exports = {
    tellAge
}