//EXERCISE 7:
//Write a function called "stringChop" that chops a string into chunks of a given length. 
//It takes two arguments: the first is a string and the second is a number, that will be the size of you chunks.
//========================= Examples ========================= 
//stringChop('BarcelonaCodeSchool') =====> ["BarcelonaCodeSchool"]
//stringChop('BarcelonaCodeSchool',0) =====> ["BarcelonaCodeSchool"]
//stringChop('BarcelonaCodeSchool',2) =====> ["Ba", "rc", "el", "on", "aC", "od", "eS", "ch", "oo", "l"] 
//stringChop('BarcelonaCodeSchool',3) =====> ["Bar", "cel", "ona", "Cod", "eSc", "hoo", "l"]

function stringChop(word, num){
    var partialResult = '';
    var finalResult = [];
    if(num == undefined || num == 0){
        return word;
    }
    for(var i = 0; i < word.length; i++){
        partialResult += word[i];
        if(partialResult.length == num){
            finalResult.push(partialResult);
            partialResult = '';
        }
    }
    if(partialResult.length > 0){
        finalResult.push(partialResult); 
    }
    return finalResult; 
}

module.exports = {
    stringChop
}