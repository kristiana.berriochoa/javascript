//EXERCISE 6:
//Let's make a currency converter! It should be a flexible function which reacts differently to different inputs.
//Outside the function, declare an array called "currenciesDB" which will hold the currencies.
//Write a function called "dinamicConverter" which takes three arguments: 
//- If the first argument is "add", then the second is an array which contains a currency and an exchange rate (to the dollar).
//- If the item is successfully added, it should return the number of added items.
//- If the first argument is "convert", then the second is an array which contains a currency and an amount to be convert.
//- The third argument will be the currency to convert your money to.
//- If the currency is not present or any of the arguments are missing, return this error message: "invalid data provided!"
//=========================
//PLease make sure that the currency is added only once so as not to overload the DB!
//If the currency is already present, return "invalid data provided!"
//Return the result of the conversion.
//========================= Examples ========================= 
//dinamicConverter('add', ['euro',1.2]) =====> 1
//dinamicConverter('add', ['euro',1.2]) =====> "invalid data provided!" (because the item is already present, from above)
//dinamicConverter('add', ['gbp',1.5]) =====> 1
//dinamicConverter('add', ['gbp',1.5]) =====> "invalid data provided!"
//dinamicConverter('convert', ['euro',100], 'gbp') =====> 80
//dinamicConverter('convert', ['euro',100], 'hmmm') =====> "invalid data provided!"

var currenciesDB = [];

function dinamicConverter(action, convertArr, money){
    console.log(action,convertArr,money)
    if(action == 'add'){
        var currExists = false;
        currenciesDB.forEach(element => {
            var curName = element[0];
            if(convertArr[0] == curName){
                currExists = true;
            } 
        })
        if(currExists == true){
            return "invalid data provided!";
        } else {
            currenciesDB.push(convertArr);
            return 1;
        }
    }
    if(action == 'convert'){
        var fromCurr = convertArr[0];
        var toCurr = money;
        var amount = convertArr[1];
        var fromRate = 0;
        var toRate = 0;
        currenciesDB.forEach(element => {
            var curName = element[0];
            if(fromCurr == curName){
                fromRate = element[1];
            } 
        })
        if(fromRate == 0){
            return 'invalid data provided!';
        } 
        currenciesDB.forEach(element => {
            var curName = element[0];
            if(toCurr == curName){
                toRate = element[1];
            } 
        })
        if(toRate == 0){
            return 'invalid data provided!';
        }
        return (amount * fromRate) / toRate;
    }
}

module.exports = {
    currenciesDB, dinamicConverter
}