//EXERCISE 3:
//Write a function called "Filter", which takes three arguments: an array, a data type and a minLength.
//Loop through the array and return a new array with the elements that are NOT the type of passed data type. 
//The elements have to have a length equal to or bigger than minLength.

var arr = [3, 2, 1, 'happy'];
var minLength = 6;
var type = "string"

function filter(arr, type, minLength){
    var arr2 = [];
    arr.forEach(element => {
        if(typeof(element) !== type){
            if(minLength == undefined || element.length >= minLength){
                arr2.push(element);
            }
        } 
    })
    return String(arr2);
}

module.exports = {
    filter
}