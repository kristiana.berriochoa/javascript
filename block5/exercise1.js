//EXERCISE 1:
//Write a function called "calc".
//Take three arguments: the first two are numbers, and the third is an arithmetic operator (+, -, *, /).
//Execute the appropriate operation according to the provided arithmetic operator.
//Make sure to test the function with all four arithmetic operations.
//Should the operator be missing, the function should return 'wrong data provided'.
//========================= Examples ========================= 
//calc(10, 2, '/') =====> 5
//calc(10, 2, '+') =====> 12
//calc(10, 2, '-') =====> 8
//calc(10, 2, '*') =====> 20

function calc(num1, num2, operator){ 
	if(operator == undefined) {
		return `wrong data provided`
	} else if(operator == '+'){
		return num1 + num2;
	} else if(operator == '-'){
		return num1 - num2;
	} else if(operator == '/'){
		return num1 / num2;
	} else if(operator == '*'){
		return num1 * num2;
	}
}

module.exports = {
    calc
}