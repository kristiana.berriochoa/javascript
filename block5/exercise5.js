//EXERCISE 5:
//Write a function called "checkAge" to check how many days there are between now and a next birthday. 
//This function will take three arguments: a month, a day, and a year.
//If the date-of-birth is today, return: "Happy birthday!" 
//If the date-of-birth has passed, return: "Sorry your birthday is passed for this year".
//If the day provided is out of range (31), or the month (12), return: "Error invalid data provided".
//Otherwise, "console.log" how many months and days there are between now and the next birthday.
//In case there is less than 1 month between now and the next birthday, ignore the month.
//For the sake of simplicity, let's assume that all months have 30 days.
//========================= Examples ========================= 
//Please bear in mind that these examples are not up-to-date (literally).
//Return: "There is / there are ## month / months and ## day / days until your birthday"
//checkAge(2, 8, 1982) =====> There are 6 days until your birthday
//checkAge(2, 9, 1982) =====> There is 1 month and 6 days until your birthday
//checkAge(2, 25, 1982) =====> Sorry your birthday is passed for this year

function checkAge(birthMonth, birthDay, birthYear){
	console.log(birthMonth, birthDay, birthYear); // Check what's being tested
	var today = new Date();
	var currentDay = today.getDate();
	var currentMonth = today.getMonth() + 1;
	var plural;
	var word;
	var pluralMonth;
    if(birthMonth == currentMonth && birthDay == currentDay){
		return "Happy Birthday";
	} else if(birthMonth > 12 || birthDay > 31 || birthMonth < 1 || birthDay < 1){
        return "Error invalid data provided";
    } else if(birthMonth == currentMonth && birthDay < currentDay || birthMonth < currentMonth){
		return "Sorry your birthday is passed for this year";
	} else if(birthMonth == currentMonth && birthDay > currentDay){ // Birthday in the same month
		var daysToBirthday = birthday - currentDay;
		if(daysToBirthday == 1){
			plural = "day";
			word = "is";
		} else {
			plural = "days";
			word = "are"
		}
		return `There ${word} ${daysToBirthday} ${plural} until your birthday`;
	} else if(birthMonth > currentMonth){ // Birthday in a different month 8/9/10/11/12
		var differenceInMonths = birthMonth - (currentMonth + 1); 
		var differenceInDays = (birthDay - currentDay) + 30;
		//var extraDays = differenceInDays + 30;
		if(differenceInMonths < 1 && differenceInDays <= 30){ // Birthday is less than a month away
			if(differenceInDays == 30){
				return `There is one month until your birthday`
			} else if(differenceInDays == 1){
				plural = "day";
				word = "is";
			} else {
				plural = "days";
				word = "are"
			}
			return `There ${word} ${differenceInDays} ${plural} until your birthday`;
		} else {
			if(differenceInMonths == 1){ // Birthday is one month away
				if(differenceInDays == 1){
					plural = "day";
				} else {
					plural = "days";
				}
				pluralMonth = "month";
				word = "is";
			} else { // Birthday is more than one month away
				if(differenceInDays == 1){
					plural = "day";
				} else {
					plural = "days";
				}
				pluralMonth = "months";
				word = "are";
			}
			return `There ${word} ${differenceInMonths} ${pluralMonth} and ${differenceInDays} ${plural} until your birthday`;
		}
	}	
}

module.exports = {
    checkAge
}

/*
function checkAge(month, day, year){
	console.log(month, day, year); // Check what's being tested
	var today = new Date();
	var birthday = new Date(2019, month-1, day); // Use '2019' for terminal, 'year' for DevTools	
	if(birthday.toDateString() === today.toDateString()){
		return "Happy Birthday";
	} else if(month > 12 || day > 31 || month < 1 || day < 1){ // Thorough check of invalid data
        return "Error invalid data provided";
	}
	var difference = birthday - today;
	if(difference < 1){
		return "Sorry your birthday is passed for this year";
	}
	var differenceInDays = Math.round(difference / 1000 / 60 / 60 / 24); // Use round to not lose days
	var differenceInMonths = Math.floor(differenceInDays / 30);
	var extraDays = differenceInDays - 30
	
	var plural;
	var word;
	var pluralMonth;
	if(differenceInDays <= 30){
    	if(differenceInDays == 1){
        	plural = "day";
        	word = "is";
    	} else {
        	plural = "days";
        	word = "are"
    	}
		return `There ${word} ${differenceInDays} ${plural} until your birthday`;
	} else {
    	if(differenceInMonths == 1){
			if(extraDays == 1){
				plural = "day";
			} else {
				plural = "days";
			}
        	pluralMonth = "month";
        	word = "is";
    	} else {
			if(extraDays == 1){
				plural = "day";
			} else {
				plural = "days";
			}
        	pluralMonth = "months";
        	word = "are";
    	}
		return `There ${word} ${differenceInMonths} ${pluralMonth} and ${extraDays} ${plural} until your birthday`;
	}
}
*/