//EXERCISE 2:
//Extend the "calc" function by adding some conditions.
//If the third argument is "/" or "*" and the second argument is not provided, the second argument should default to 1.
//If the third argument is "+" or "-" and the second argument is not provided, the second argument should default to 0.
//========================= Examples ========================= 
//calc(10, "/") =====> 10
//calc(30, "*") =====> 30
//calc(2, "+") =====> 2
//calc(3, "-") =====> 3

function calc(num1, num2, operator){
	if((num2 == '+' || num2 == '-') && operator == undefined){
		operator == num2;
		num2 = 0;
		return num1 + num2 || num1 - num2;
	} else if((num2 == '*' || num2 == '/') && operator == undefined){
		operator == num2;
		num2 = 1;
		return num1 * num2 || num1 / num2;
	}
	if(operator == undefined) {
		return `wrong data provided`;
	} else if(operator == '+'){
		return num1 + num2;
	} else if(operator == '-'){
		return num1 - num2;
	} else if(operator == '/'){
		return num1 / num2;
	} else if(operator == '*'){
		return num1 * num2;
	}
}

module.exports = {
     calc
}