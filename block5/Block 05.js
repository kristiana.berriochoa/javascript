//************************************* BLOCK 5 ******************************************

//EXERCISE 1:
//Write a function called "calc".
//Take three arguments: the first two are numbers, and the third is an arithmetic operator (+, -, *, /).
//Execute the appropriate operation according to the provided arithmetic operator.
//Make sure to test the function with all four arithmetic operations.
//Should the operator be missing, the function should return 'wrong data provided'.
//========================= Examples ========================= 
//calc(10, 2, '/') =====> 5
//calc(10, 2, '+') =====> 12
//calc(10, 2, '-') =====> 8
//calc(10, 2, '*') =====> 20


//EXERCISE 2:
//Extend the "calc" function by adding some conditions.
//If the third argument is "/" or "*" and the second argument is not provided, the second argument should default to 1.
//If the third argument is "+" or "-" and the second argument is not provided, the second argument should default to 0.
//========================= Examples ========================= 
//calc(10, "/") =====> 10
//calc(30, "*") =====> 30
//calc(2, "+") =====> 2
//calc(3, "-") =====> 3


//EXERCISE 3:
//Write a function called "Filter", which takes three arguments: an array, a data type and a minLength.
//Loop through the array and return a new array with the elements that are NOT the type of passed data type. 
//The elements have to have a length equal to or bigger than minLength.


//EXERCISE 4:
//Write a function called "tellAge" t0 tell you how old you are, based on a date-of-birth passed.
//This function will take three arguments: a month, a day, and a year.
//You should use the "Date" object to set the value of today.
//If the birth-date is less than one year from the current date, return: "you are NUMBER_DAYS old"
//If it's more than one year, return: "you are NUMBER_YEARS old"
//Date object: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date
//========================= Examples ========================= 
//Assuming today is April 11, 2018:
//tellAge(8, 2, 1982) =====> you are 35 years old
//tellAge(4, 3, 1982) =====> you are 36 years old
//tellAge(4, 9, 2018) =====> you are 2 days old


//EXERCISE 5:
//Write a function called "checkAge" to check how many days there are between now and a next birthday. 
//This function will take three arguments: a month, a day, and a year.
//If the date-of-birth is today, return: "Happy birthday!" 
//If the date-of-birth has passed, return: "Sorry your birthday is passed for this year".
//If the day provided is out of range (31), or the month (12), return: "Error invalid data provided".
//Otherwise, "console.log" how many months and days there are between now and the next birthday.
//In case there is less than 1 month between now and the next birthday, ignore the month.
//For the sake of simplicity, let's assume that all months have 30 days.
//========================= Examples ========================= 
//Please bear in mind that these examples are not up-to-date (literally).
//Return: "There is / there are ## month / months and ## day / days until your birthday"
//=========================
//checkAge(2, 8, 1982) =====> There are 6 days until your birthday
//checkAge(2, 9, 1982) =====> There is 1 month and 6 days until your birthday
//checkAge(2, 25, 1982) =====> Sorry your birthday is passed for this year


//EXERCISE 6:
//Let's make a currency converter! It should be a flexible function which reacts differently to different inputs.
//Outside the function, declare an array called "currenciesDB" which will hold the currencies.
//Write a function called "dinamicConverter" which takes three arguments: 
//- If the first argument is "add", then the second is an array which contains a currency and an exchange rate (to the dollar).
//- If the item is successfully added, it should return the number of added items.
//- If the first argument is "convert", then the second is an array which contains a currency and an amount to be convert.
//- The third argument will be the currency to convert your money to.
//- If the currency is not present or any of the arguments are missing, return this error message: "invalid data provided!"
//=========================
//PLease make sure that the currency is added only once so as not to overload the DB!
//If the currency is already present, return "invalid data provided!"
//Return the result of the conversion.
//========================= Examples ========================= 
//dinamicConverter('add', ['euro',1.2]) =====> 1
//dinamicConverter('add', ['euro',1.2]) =====> "invalid data provided!" (because the item is already present, from above)
//dinamicConverter('add', ['gbp',1.5]) =====> 1
//dinamicConverter('add', ['gbp',1.5]) =====> "invalid data provided!"
//dinamicConverter('convert', ['euro',100], 'gbp') =====> 80
//dinamicConverter('convert', ['euro',100], 'hmmm') =====> "invalid data provided!"


//EXERCISE 7:
//Write a function called "stringChop" that chops a string into chunks of a given length. 
//It takes two arguments: the first is a string and the second is a number, that will be the size of you chunks.
//========================= Examples ========================= 
//stringChop('BarcelonaCodeSchool') =====> ["BarcelonaCodeSchool"]
//stringChop('BarcelonaCodeSchool',0) =====> ["BarcelonaCodeSchool"]
//stringChop('BarcelonaCodeSchool',2) =====> ["Ba", "rc", "el", "on", "aC", "od", "eS", "ch", "oo", "l"] 
//stringChop('BarcelonaCodeSchool',3) =====> ["Bar", "cel", "ona", "Cod", "eSc", "hoo", "l"]


//EXERCISE 8:
//Write a function called "strCut".
//It takes three arguments: a string and two indexes.
//Return a string after removing the letters of the two given indexes.
//========================= Examples ========================= 
//strCut('Antonello', 0,8) =====> "ntonell"
//strCut('Antonello', 3,5) =====> "Antnllo"
//strCut('Antonello', 2,8) =====> "Anonell"


//EXTRA CHALLENGE: Rock-paper-scissors
//Requirements:
//The computer chooses randomly on every turn.
//The player can click on a picture representing an option and choose it.
//You should see if you won or lost after every turn.
//In order to win the match, the player or computer needs to win twice.
//There needs to be a score keeper to show the current score of the player and computer.
//In the case that both select the same option, it's a draw and no points are given out.
//=========================
//*** Your solution goes in the Rock_paper_scissors folder, if you are going to add UI ***