//EXERCISE 1:
//Create a function called "firstLoop".
//Outside the function, declare a variable called "i" and give it a value of 1.
//Inside the function, create a for-loop that prints out the numbers from 1 to 10.
//Return "i".

var i = 1;

function firstLoop(){
  for(i; i <= 10; i++){
    console.log(i);
  }
  return i;
}

module.exports = {
  firstLoop, i
}