//EXERCISE 12:
//Write a function called "reverser".
//It takes one sting as an argument and returns the same string in reverse.
//You must use: a loop, push and join.
//========================= Example =========================
//var str = 'reeb dloc fo tnip ecin a htiw dna oyam htiw seotatop deirf peed evol I'
//Expected output =====> 'I love deep fried potatoes with mayo and with a nice pint of cold beer'

var str = 'reeb dloc fo tnip ecin a htiw dna oyam htiw seotatop deirf peed evol I';

function reverser(str){
	var newArr = [];
	for(var i = str.length; i >= 0; i--){
		newArr.push(str[i]);
	}
	return newArr.join('');
}

module.exports = {
    reverser
}