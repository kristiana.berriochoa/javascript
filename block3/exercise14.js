//EXERCISE 14:
//Write a function called "budgetTracker", which helps track your expenses while on vacation...
//You went on vacation to Japan and now you need to check your finances a bit.
//To do that, you need to find out the average expense per day in dollars. 
//Each element of the array will be the daily expense in Japanese yens.
//The conversion rate for yen to dollars is 0.0089.
//Return the daily average.
//========================= Example =========================
//Monday - Sunday:
//var arr = ['10003', '46733', '45833', '3534', '57354', '45334', '34856'];
//Expected output =====> 310

var arr = ['10003', '46733', '45833', '3534', '57354', '45334', '34856'];

function budgetTracker(arr){
    var total = 0;
    var final = 0;
    arr.forEach(element => {
        total = total + parseInt(element); // parseInt() converts a string to an integer
    })
    final = (total / 7) * 0.0089;
    return Math.round(final); // Math.round()returns number rounded to nearest integer
}

module.exports = {
    budgetTracker
}