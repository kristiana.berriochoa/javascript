//EXERCISE 7:
//Write a function called "multy". 
//It takes an array as an argument, multiplies each number, and then returns the result.
//========================= Example =========================
//multy([10,10]) =====> returns 100

var arr = [1, 2, 3, 4];

function multy(arr){
	var total = 1;
	for(var i = 0; i < arr.length; i++){
		total = total * arr[i];
    }
	return total;
}

module.exports = {
    multy
}