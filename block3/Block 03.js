//************************************* BLOCK 3 ******************************************

//EXERCISE 1:
//Create a function called "firstLoop".
//Outside the function, declare a variable called "i" and give it a value of 1.
//Inside the function, create a for-loop that prints out the numbers from 1 to 10.
//Return "i".


//EXERCISE 2:
//Create a function called "firstLoopReverse".
//Outside the function, declare a variable called "i" and give it a value of 11.
//Inside the function, create a for-loop that prints out the numbers from 11 to 1.
//Return "i".


//EXERCISE 3:
//Write a function called "isEven".
//It takes an array as an argument and checks if each element is even, or odd.
//Inside the function, declare a variable called "count" which keeps track of even numbers.
//Return count.
//========================= Example =========================
//See the example below for use of a basic "if statement":
/*function isEven(){
    Declare your count variable;
    For-loop and inside it...
        if("your_condition_here"){
           count++;
    }
    return count;
}*/


//EXERCISE 4:
//Declare the following array: var arr = ['one','two','three','four']
//Create a function called "looper" which takes the array as an argument.
//Inside the function, write a forEach-loop and console.log each element and index for every iteration.
//Also, declare a variable called "count" and increment it by one for each iteration.
//Return count.


//EXERCISE 5:
//Write a function called "isString" which takes an array as an argument.
//Inside the function, write a forEach-loop that loops through an array and checks if each element is a string. 
//Every time it finds a string, it will push it to a new array.
//Return the new array.
//==================================================
//Refer to the example in EXERCISE 3 for a simple if-statement, if you wish.


//EXERCISE 6:
//Write a function called "sum".
//It takes an array as an argument and returns the sum of all its elements.
//========================= Example =========================
//sum([10,10]) =====> returns 20


//EXERCISE 7:
//Write a function called "multy". 
//It takes an array as an argument, multiplies each number, and then returns the result.
//========================= Example =========================
//multy([10,10]) =====> returns 100


//EXERCISE 8:
//Write a function called "timesTwo", which takes an array as an argument.
//Inside the function, use a forEach-loop and push the numbers to a new array.
//Multiply them by two on the way!
//========================= Example =========================
//timesTwo([2, 3, 65, 22]) =====> returns [4, 6, 130, 44]


//EXERCISE 9:
//Write a function called "twoArrays", which takes two arrays as arguments. 
//These arrays are the same length.
//Inside the function, declare a variable called count.
//Loop and compare each element of one array with that of the other, in pairs with the same indices.
//Every time it finds a match, increase the number of count by one.
//Return count.


//EXERCISE 10:
//Now, do the same exercise, only this time ignore the type.


//EXERCISE 11:
//Write a function called "lowerCaseLetters", which takes a string as an argument.
//The string will contain some upper case letters and some numbers. 
//Create a variable which needs to contain all the lowercase letters of the original string.
//Return the resulted string. 
//========================= Example =========================
//var str = 'An2323t2323one32llo123455Likes567323Play323ing567243G2323a3mes345';
//Expected output =====> 'antonello likes playing games';


//EXERCISE 12:
//Write a function called "reverser".
//It takes one sting as an argument and returns the same string in reverse.
//You must use: a loop, push and join.
//========================= Example =========================
//var str = 'reeb dloc fo tnip ecin a htiw dna oyam htiw seotatop deirf peed evol I'
//Expected output =====> 'I love deep fried potatoes with mayo and with a nice pint of cold beer'


//EXERCISE 13:
//Create a function called "shortener", which takes a string as an argument.
//This string will be a full name like "Antonello Sanna".
//Convert the name to an abbreviated form (initials).
//Take the first letter of the surname, make sure it's capitalized, and add a dot at the end.
//========================= Examples =========================
//var str = 'Ada lovelace' =====> "Ada L." 
//var str = 'Antonello Sanna' =====> Antonello S.


//EXERCISE 14:
//Write a function called "budgetTracker", which helps track your expenses while on vacation...
//You went on vacation to Japan and now you need to check your finances a bit.
//To do that, you need to find out the average expense per day in dollars. 
//Each element of the array will be the daily expense in Japanese yens.
//The conversion rate for yen to dollars is 0.0089.
//Return the daily average.
//========================= Example =========================
//Monday - Sunday:
//var arr = ['10003', '46733', '45833', '3534', '57354', '45334', '34856'];
//Expected output =====> 310