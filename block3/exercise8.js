//EXERCISE 8:
//Write a function called "timesTwo", which takes an array as an argument.
//Inside the function, use a forEach-loop and push the numbers to a new array.
//Multiply them by two on the way!
//========================= Example =========================
//timesTwo([2, 3, 65, 22]) =====> returns [4, 6, 130, 44]

var arr = [1, 2, 3, 4];

function timesTwo(arr){
	var arr2 = [];
	arr.forEach(function(element, index){
        arr2.push(element * 2);
    });
	return arr2;
}

module.exports = {
    timesTwo
}