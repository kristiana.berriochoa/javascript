//EXERCISE 9:
//Write a function called "twoArrays", which takes two arrays as arguments. 
//These arrays are the same length.
//Inside the function, declare a variable called count.
//Loop and compare each element of one array with that of the other, in pairs with the same indices.
//Every time it finds a match, increase the number of count by one.
//Return count.

var arr = [1, 2, 3, 4];
var arr2 = [5, 6, 7, 8];

function twoArrays(arr, arr2){
	var count = 0;
    arr.forEach(function(item, idx){
		if(arr[idx] === arr2[idx]){
			count++;
		}	    
	})
   return count;
}

module.exports = {
    twoArrays
}