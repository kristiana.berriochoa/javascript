//EXERCISE 4:
//Declare the following array: var arr = ['one', 'two', 'three', 'four']
//Create a function called "looper" which takes the array as an argument.
//Inside the function, write a forEach-loop and console.log each element and index for every iteration.
//Also, declare a variable called "count" and increment it by one for each iteration.
//Return count.

var arr = ['one', 'two', 'three', 'four'];

function looper(arr){
	var count = 0;
	arr.forEach(function(element, index){
		console.log(element, index);
		count++;
	})
	return count;
}

module.exports = { 
    looper, arr
}

/*
function looper(arr){
	var count = 0;
	for(var i = 0; i < arr.length; i++){
		count++;   
	};
	arr.forEach(function(element, index){
		console.log(element, index);
	})
	return count;
}*/