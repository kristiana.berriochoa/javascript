//EXERCISE 2:
//Create a function called "firstLoopReverse".
//Outside the function, declare a variable called "i" and give it a value of 11.
//Inside the function, create a for-loop that prints out the numbers from 11 to 1.
//Return "i".

var i = 11;

function firstLoopReverse(){
	for(i; i >= 1; i--){
		console.log(i);
    }
	return i;
}

module.exports = {
    firstLoopReverse, i
}