//EXERCISE 3:
//Write a function called "isEven".
//It takes an array as an argument and checks if each element is even, or odd.
//Inside the function, declare a variable called "count" which keeps track of even numbers.
//Return count.
//========================= Example =========================
//See the example below for use of a basic "if statement":
/*function isEven(){
    Declare your count variable;
    For-loop and inside it...
        if("your_condition_here"){
           count++;
    }
    return count;
}*/

var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

function isEven(arr){
	var count = 0;
	for(var i = 0; i < arr.length; i++){
		if(arr[i] % 2 == 0)
		count++;
    }
	return count;
}

module.exports = {
    isEven
}