//EXERCISE 5:
//Write a function called "isString" which takes an array as an argument.
//Inside the function, write a forEach-loop that loops through an array and checks if each element is a string. 
//Every time it finds a string, it will push it to a new array.
//Return the new array.
//==================================================
//Refer to the example in EXERCISE 3 for a simple if-statement, if you wish.

var arr = ["one", 2 ,"three", 4];

function isString(arr){
    var newArr = [];
    arr.forEach(element => {
        if(typeof(element) === "string")
		    newArr.push(element);
    });
    return newArr;
}

module.exports = {
    isString
}