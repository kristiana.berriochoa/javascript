//EXERCISE 10:
//Now, do the same exercise, only this time ignore the type.

var arr = [1, 2, 3, 4];
var arr2 = [5, 6, 7, 8];

function twoArrays(arr, arr2){
	var count = 0;
    arr.forEach(function(item, idx) {
		if(arr[idx] == arr2[idx]) {
			count++;
		}
	})
   return count;
}

module.exports = {
    twoArrays
}