//EXERCISE 11:
//Write a function called "lowerCaseLetters", which takes a string as an argument.
//The string will contain some upper case letters and some numbers. 
//Create a variable which needs to contain all the lowercase letters of the original string.
//Return the resulted string. 
//========================= Example =========================
//var str = 'An2323t2323one32llo123455Likes567323Play323ing567243G2323a3mes345';
//Expected output =====> 'antonello likes playing games';

var str = 'An2323t2323one32llo123455Likes567323Play323ing567243G2323a3mes345';

function lowerCaseLetters(str){
    var newStr = [];
    var arr = str.split('');
    arr.forEach(function(item, idx){
        if(isNaN(item)){
            if(item === item.toUpperCase()){
                newStr.push(' ')
            }
            newStr.push(item.toLowerCase());
        }
    })
   return newStr.join('').trim();
}

module.exports = {
	lowerCaseLetters
}