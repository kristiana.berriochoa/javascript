//EXERCISE 13:
//Create a function called "shortener", which takes a string as an argument.
//This string will be a full name like "Antonello Sanna".
//Convert the name to an abbreviated form (initials).
//Take the first letter of the surname, make sure it's capitalized, and add a dot at the end.
//========================= Examples =========================
//var str = 'Ada lovelace' =====> "Ada L." 
//var str = 'Antonello Sanna' =====> Antonello S.

var str = "Antonello sanna";

function shortener(str){
    var str2 = str.split(' '); // String.split() divides string and puts substrings in an array
    var nameF = str2[0].charAt(0).toUpperCase() + str2[0].slice(1); // String.charAt() is "character at"
	var nameL = str2[1].slice(0, 1).toUpperCase() + '.'; 
   	return nameF + (' ').concat(nameL);
 }

module.exports = {
    shortener
}