//EXERCISE 6:
//Write a function called "sum".
//It takes an array as an argument and returns the sum of all its elements.
//========================= Example =========================
//sum([10,10]) =====> returns 20

var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

function sum(arr){
	var total = 0;
	for(var i = 0; i < arr.length; i++){
		total = total + arr[i];
    }
	return total;
}

module.exports = {
    sum
}