//EXERCISE 11:
//Create a function called "isThere" which takes an array as the first argument and a string as the second. 
//It needs to check if the string is inside the array and return true if it is, or false if it is not.
//========================= Example =========================
//var arr = ['green','red','black','blue','brown','yellow','purple']
//isThere('red') =====> true
//isThere('banana') =====> false

var arr = ['green','red','black','blue','brown','yellow','purple'];
var str = 'red';

function isThere(arr, str){
    return arr.includes(str);
}

module.exports ={
    arr, isThere
}