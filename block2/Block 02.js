//************************************* BLOCK 2 ******************************************

//EXERCISE 1:
//Write a function called "assigner" which takes two arguments: an array and an index.
//Take one element from the provided array, at the given index, and place it in a new array.
//Then return it.
//========================= Example =========================
//var arr = ['milk', 'cheese', 'car', 'lime'];
//var index = 2; 
//assigner(arr, 2) =====> ['car']


//EXERCISE 2:
//Write a function called "takeAll" which takes an array as an argument. 
//Return a new array which has all the values of the original array, but in reverse order.
//========================= Example =========================
//var arr = ['milk', 'cheese', 'car', 'lime'];
//takeAll(arr) =====> ["lime", "car", "cheese", "milk"]


//EXERCISE 3:
//Create a function called "swap" which takes two arrays as the first two arguments and a position, which is an index, as the third.
//Return the two arrays, swapping the values in each array at the position passed as an argument.
//Return both arrays inside another array, as seen in the example:
//========================= Example =========================
//var arr = ['banana','apple','orange'];
//var arr2 = ['tv','dvd-player','playstation'];
//var pos = 2;
//swap(arr, arr2, pos) =====> [["banana", "apple", "playstation"] , ["tv", "dvd-player", "orange"]]


//EXERCISE 4:
//Create a function called "splicer" which removes an element from an array and returns another array with the removed element.
//This function takes two arguments: 
//1) An array 
//2) A position/index of an element to be removed from the array
//========================= Example =========================
//var arr = ['cheese','salame','bread','water','pizza']
//splicer(arr,2) =====> ['cheese','salame','water','pizza']


//EXERCISE 5:
//Write a function called "removeFirstAndLast".
//It takes an array as an argument and returns an array of which the first and last elements have been removed.
//========================= Example =========================
//var arr = ['car','soap','banana','tv','toothbrush']
//removeFirstAndLast(arr) =====> ['soap','banana','tv']


//EXERCISE 6:
//Write a function called "removeAll". 
//It takes an array as an argument and returns an array of which all elements have be removed.
//========================= Example =========================
//var arr = [1,2,3,4,5,6,7,8,9,0,3,4,523,44,3454]
//removeAll(arr) =====> []


//EXERCISE 7:
//Write a function called "pusher". It takes one argument: an array.
//Using push take all elements from the array and push them to a new variable called "arr2" to avoid nested arrays.
//Return arr2.
//========================= Example =========================
//var arr = ["one","two","three","four"]
//pusher(arr) =====> ["one","two","three","four"]


//EXERCISE 8:
//Create a function called "take_and_push" which takes three arguments: an array and two numbers.
//The second and third arguments are indices.
//You need to take the elements in the array at the given indices and push them to a new array.
//Return the new array.
//========================= Example =========================
//var arr = ['Breaking bad','WestWorld','Psych','Games of Thrones','Gotham','Spartacus','Dexter','Office']
//take_and_push(arr, 2, 4) =====> ["Psych","Gotham"]


//EXERCISE 9:
//Create a function called "concatenator" which takes an array as an argument.
//Then using Array.concat it returns a new array which is a clone of the original array.
//========================= Example =========================
//var arr = ['Breaking bad','WestWorld','Psych','Games of Thrones','Gotham','Spartacus','Dexter','Office']
//concatenator(arr) =====> ['Breaking bad','WestWorld','Psych','Games of Thrones','Gotham','Spartacus','Dexter','Office']


//EXERCISE 10:
//Create a function called "findPosition" which takes two arguments: an array and an element.
//Return the index of the given element.
//========================= Example =========================
//var arr = ['glass','car','watch','sofa','macbook']
//findPosition('car') =====> 1
//findPosition('sofa') =====> 3
//findPosition('glass') =====> 0


//EXERCISE 11:
//Create a function called "isThere" which takes an array as the first argument and a string as the second. 
//It needs to check if the string is inside the array and return true if it is, or false if it is not.
//========================= Example =========================
//var arr = ['green','red','black','blue','brown','yellow','purple']
//isThere('red') =====> true
//isThere('banana') =====> false


//EXERCISE 12:
//Do the same exercise, but using the method "includes".
//Only this time you should return false if the color is there, and true if it's not!
//========================= Example =========================
//var arr = ['green','red','black','blue','brown','yellow','purple']
//isThere('red') =====> false
//isThere('banana') =====> true


//EXERCISE 13:
//Write a function called "characterRemover" which takes two arguments: a string and a character to be removed. 
//Return the string free of the unwanted character.
//Please note: in order for the string to make sense you should replace the unwanted character with a space so that the string looks like the example:
//========================= Example =========================
//var str = 'I,Really,Like,Pizza'; expected output =====> I Really Like Pizza


//EXERCISE 14
//Write a function called "isArrayFunc" which takes an argument of anything.
//It returns true if it's an array, and false if it's anything else.
//========================= Example =========================
//var one = {name:'antonello'} =====> false
//var two = ['name', 'antonello'] =====> true
//var three = [[],[],{},"antonello",3,function(){}] =====> true