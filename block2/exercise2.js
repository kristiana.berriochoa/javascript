//EXERCISE 2:
//Write a function called "takeAll" which takes an array as an argument. 
//Return a new array which has all the values of the original array, but in reverse order.
//========================= Example =========================
//var arr = ['milk','cheese','car','lime'];
//takeAll(arr) =====> ["lime", "car", "cheese", "milk"]

var arr = ['milk', 'cheese', 'car', 'lime'];

function takeAll(arr){
    arr.reverse();
    return arr;
}

module.exports ={
    takeAll
}