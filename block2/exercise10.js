//EXERCISE 10:
//Create a function called "findPosition" which takes two arguments: an array and an element.
//Return the index of the given element.
//========================= Example =========================
//var arr = ['glass','car','watch','sofa','macbook']
//findPosition('car') =====> 1
//findPosition('sofa') =====> 3
//findPosition('glass') =====> 0

var arr = ['glass','car','watch','sofa','macbook'];
var element = 'car';

function findPosition(arr, element){
	return arr.indexOf(element);
}

module.exports = {
    arr, findPosition
}