//EXERCISE 3:
//Create a function called "swap" which takes two arrays as the first two arguments and a position, which is an index, as the third.
//Return the two arrays, swapping the values in each array at the position passed as an argument.
//Return both arrays inside another array, as seen in the example:
//========================= Example =========================
//var arr = ['banana','apple','orange'];
//var arr2 = ['tv','dvd-player','playstation'];
//var pos = 2;
//swap(arr, arr2, pos) =====> [["banana", "apple", "playstation"] , ["tv", "dvd-player", "orange"]]

var arr = ['banana', 'apple', 'orange'];
var arr2 = ['tv','dvd-player','playstation'];
var pos = 2;

function swap(arr, arr2, pos){
	var helper = arr[pos];
	arr[pos] = arr2[pos];
	arr2[pos] = helper;
    var arr3 = [];
    arr3 = [arr, arr2]
	return arr3;
}

module.exports = { swap }