//EXERCISE 8:
//Create a function called "take_and_push" which takes three arguments: an array and two numbers.
//The second and third arguments are indices.
//You need to take the elements in the array at the given indices and push them to a new array.
//Return the new array.
//========================= Example =========================
//var arr = ['Breaking bad','WestWorld','Psych','Games of Thrones','Gotham','Spartacus','Dexter','Office']
//take_and_push(arr, 2, 4) =====> ["Psych","Gotham"]

var arr = ['Breaking bad','WestWorld','Psych','Games of Thrones','Gotham','Spartacus','Dexter','Office'];
var pos = 2;
var pos2 = 4;

function take_and_push(arr, pos, pos2){
	var arr2 = [];
	arr2[0] = arr[pos];
	arr2[1] = arr[pos2];
	return arr2;
}

module.exports = {
    take_and_push
}
