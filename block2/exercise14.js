//EXERCISE 14
//Write a function called "isArrayFunc" which takes an argument of anything.
//It returns true if it's an array, and false if it's anything else.
//========================= Example =========================
//var one = {name:'antonello'} =====> false
//var two = ['name', 'antonello'] =====> true
//var three = [[],[],{},"antonello",3,function(){}] =====> true

var one = {name: 'antonello'};
var two = ['name', 'antonello'];
var three = [[], [], {}, "antonello", 3, function(){}];

function isArrayFunc(one){
	return Array.isArray(one);
}

module.exports = {
    isArrayFunc
}