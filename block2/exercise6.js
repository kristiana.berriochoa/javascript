//EXERCISE 6:
//Write a function called "removeAll". 
//It takes an array as an argument and returns an array of which all elements have be removed.
//========================= Example =========================
//var arr = [1,2,3,4,5,6,7,8,9,0,3,4,523,44,3454]
//removeAll(arr) =====> []

var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 3, 4, 523, 44, 3454];

function removeAll(arr){
	arr.splice(0, arr.length);
	return arr;
}

module.exports = {
    removeAll
}