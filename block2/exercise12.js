//EXERCISE 12:
//Do the same exercise, but using the method "includes".
//Only this time you should return false if the color is there, and true if it's not!
//========================= Example =========================
//var arr = ['green','red','black','blue','brown','yellow','purple']
//isThere('red') =====> false
//isThere('banana') =====> true

var arr = ['green','red','black','blue','brown','yellow','purple'];
var str = 'red';

function isThere(arr, str){
	if(arr.includes(str)){
		return false;
    } else {
		return true;
	}
}

module.exports ={
    arr, isThere
}