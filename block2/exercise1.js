//EXERCISE 1:
//Write a function called "assigner" which takes two arguments: an array and an index.
//Take one element from the provided array, at the given index, and place it in a new array.
//Then return it.
//========================= Example =========================
//var arr = ['milk', 'cheese', 'car', 'lime'];
//var index = 2; 
//assigner(arr, 2) =====> ['car']

var arr = ['milk', 'cheese', 'car', 'lime'];
console.log(arr.length)
var index = 2;

function assigner(arr, index){
    var arr2 = [];
    arr2[0] = arr[index];
    return arr2;
}

//Do not modify module.export!
module.exports = {
    arr, assigner, index
}