//EXERCISE 4:
//Create a function called "splicer" which removes an element from an array and returns another array with the removed element.
//This function takes two arguments: 
//1) An array 
//2) A position/index of an element to be removed from the array
//========================= Example =========================
//var arr = ['cheese','salame','bread','water','pizza']
//splicer(arr,2) =====> ['cheese','salame','water','pizza']

var arr = ['cheese','salame','bread','water','pizza'];
var pos = 2;

function splicer(arr, pos){
    arr.splice(pos, 1);
    return arr;
}

module.exports = {
    splicer
}