//EXERCISE 13:
//Write a function called "characterRemover" which takes two arguments: a string and a character to be removed. 
//Return the string free of the unwanted character.
//Please note: in order for the string to make sense you should replace the unwanted character with a space so that the string looks like the example:
//========================= Example =========================
//var str = 'I,Really,Like,Pizza'; expected output =====> I Really Like Pizza

var str = 'I,Really,Like,Pizza';
var ele = ' ';

function characterRemover(str, ele){
	return str.split(ele).join(" ")
}

module.exports = {
    str, characterRemover
}