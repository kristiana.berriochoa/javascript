//EXERCISE 9:
//Create a function called "concatenator" which takes an array as an argument.
//Then using Array.concat it returns a new array which is a clone of the original array.
//========================= Example =========================
//var arr = ['Breaking bad','WestWorld','Psych','Games of Thrones','Gotham','Spartacus','Dexter','Office']
//concatenator(arr) =====> ['Breaking bad','WestWorld','Psych','Games of Thrones','Gotham','Spartacus','Dexter','Office']

var arr = ['Breaking bad','WestWorld','Psych','Games of Thrones','Gotham','Spartacus','Dexter','Office'];

function concatenator(arr){
	var arr2 = []; 
	arr2 = arr.concat();
	return arr2;
}

module.exports = {
    concatenator
}