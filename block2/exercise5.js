//EXERCISE 5:
//Write a function called "removeFirstAndLast".
//It takes an array as an argument and returns an array of which the first and last elements have been removed.
//========================= Example =========================
//var arr = ['car','soap','banana','tv','toothbrush']
//removeFirstAndLast(arr) =====> ['soap','banana','tv']

var arr = ['car', 'soap', 'banana', 'tv', 'toothbrush'];

function removeFirstAndLast(arr){
	arr.splice(0, 1);
	arr.splice(-1);
	return arr;
}

module.exports = {
    removeFirstAndLast
}