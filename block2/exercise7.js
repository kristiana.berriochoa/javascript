//EXERCISE 7:
//Write a function called "pusher". It takes one argument: an array.
//Using push take all elements from the array and push them to a new variable called "arr2" to avoid nested arrays.
//Return arr2.
//========================= Example =========================
//var arr = ["one","two","three","four"]
//pusher(arr) =====> ["one","two","three","four"]

var arr = ["one", "two", "three", "four"];

function pusher(arr){
	var arr2 = [];
	arr2.push(arr);
	return arr2.toString().split(",");
}

module.exports = { pusher };